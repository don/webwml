#use wml::debian::translation-check translation="de5ae73bc2b4139bec8bd6accc52e619a842ca38" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>python-reportlab, une bibliothèque Python pour créer des documents PDF,
est prédisposée à une vulnérabilité d'injection de code lors de l'analyse
d'un attribut « color ». Un attaquant peut tirer avantage de ce défaut pour
exécuter du code arbitraire lors du traitement d'un document contrefait
pour l'occasion.</p>

<p>Pour la distribution oldstable (Stretch), ce problème a été corrigé dans
la version 3.3.0-2+deb9u1.</p>

<p>Pour la distribution stable (Buster), ce problème a été corrigé dans la
version 3.5.13-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-reportlab.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python-reportlab,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python-reportlab">\
https://security-tracker.debian.org/tracker/python-reportlab</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4663.data"
# $Id: $
