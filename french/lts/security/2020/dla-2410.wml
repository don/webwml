#use wml::debian::translation-check translation="af984639b9f176ab5f37bce4400bc803b8754c92" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité de double libération de
zone de mémoire dans BlueZ, une suite d’outils, d’utilitaires et de démons
Bluetooth.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27153">CVE-2020-27153</a>

<p>Dans BlueZ avant la version 5.55, une double libération de zone de mémoire a
été découverte dans la routine disconnect_cb() de gatttool dans
shared/att.c. Un attaquant distant pourrait éventuellement causer un déni de
service ou une exécution de code, lors de la découverte de service, à cause d’un
évènement de gestion de déconnexion redondante.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 5.43-2+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bluez.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2410.data"
# $Id: $
