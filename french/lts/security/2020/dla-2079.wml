#use wml::debian::translation-check translation="04c5e883b5d9a0803379d815a8c37a2285aa888a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans le paquet otrs2 qui
pourraient conduire à un accès non autorisé, une exécution de code à distance et
une usurpation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1765">CVE-2020-1765</a>

<p>Un contrôle incorrect de paramètres permet l’usurpation des champs from
des écrans suivants : AgentTicketCompose, AgentTicketForward, AgentTicketBounce.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1766">CVE-2020-1766</a>

<p>À cause d’un traitement incorrect d’images téléchargées, il est possible dans
des conditions très improbables et rares d’obliger le navigateur des agents
à exécuter du javascript malveillant à partir d’un fichier SVG spécialement
contrefait, rendu comme un fichier jpg interne.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1767">CVE-2020-1767</a>

<p>À partir de vues non autorisées de brouillons, le texte pouvait être modifié
complètement et envoyer au nom du propriétaire du brouillon. Pour le client, il
n’était pas visible que le message était envoyé par un autre agent.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.3.18-1+deb8u13.</p>

<p>Nous vous recommandons de mettre à jour vos paquets otrs2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2079.data"
# $Id: $
