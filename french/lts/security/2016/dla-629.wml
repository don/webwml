#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Lukas Reschke a découvert que Jackrabbit d'Apache, une implémentation de
dépôt de contenu (« Content Repository ») pour Java, était vulnérable à des
attaques par contrefaçon de requête intersite (CSRF) dans le module webdav
de Jackrabbit.</p>

<p>La vérification par la CSRF de Content-Type des requêtes POST ne gérait
pas l'absence de champs d'en-tête Content-Type, pas plus que les variations
dans la valeur du champ en rapport avec la casse (majuscule/minuscule) ou
des paramètres optionnels. Cela pourrait être exploité pour créer une
ressource au moyen d'une CSRF.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 2.3.6-1+deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jackrabbit.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-629.data"
# $Id: $
