#use wml::debian::translation-check translation="cea2e92155b7909ecf57574f339e401f9ed25744" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Squid, un serveur
mandataire et de cache.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28651">CVE-2021-28651</a>

<p>À cause d’un bogue de gestion de tampon, un déni de service est possible.
Lors de la résolution d’une requête avec l’URN : scheme, l’analyseur laisse
fuiter une petite partie de la mémoire. Cependant, il existe une méthodologie
d’attaque non précisée qui peut facilement provoquer une grande consommation
de mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28652">CVE-2021-28652</a>

<p>À cause d’une validation incorrecte d’analyseur, une attaque par déni de
service est possible sur l’API de gestion de cache. Cela permet à un client
authentifié de déclencher une fuite de mémoire qui, dans la durée, conduit à un
déni de service à l'aide d'une courte chaîne non précisée de requête . Cette
attaque est limitée aux clients pouvant accéder à cette API.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31806">CVE-2021-31806</a>

<p>À cause d’un bogue de gestion de mémoire, une vulnérabilité d’attaque par
déni de service (contre tous les clients utilisant le mandataire) est
possible à l’aide du traitement d'une requête d’intervalles HTTP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31807">CVE-2021-31807</a>

<p>Un problème de dépassement d'entier permet à un serveur distant de réaliser
un déni de service lors de la fourniture de réponses aux requêtes d’intervalle
HTTP. Le déclencheur du problème est un en-tête pouvant être attendu dans un
trafic HTTP sans aucune intention malveillante.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31808">CVE-2021-31808</a>

<p>À cause d’un bogue de validation d’entrée, une vulnérabilité d’attaque par
déni de service (contre tous les clients utilisant le mandataire) est
possible à l’aide de l'envoi d'une requête d’intervalles HTTP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33620">CVE-2021-33620</a>

<p>Un serveur distant peut provoquer un déni de service (affectant la
disponibilité à tous les clients) à l'aide d'une réponse HTTP. Le déclencheur du
problème est un en-tête pouvant être attendu dans un trafic HTTP sans aucune
intention malveillante du serveur.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 3.5.23-5+deb9u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets squid3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de squid3, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/squid3">\
https://security-tracker.debian.org/tracker/squid3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2685.data"
# $Id: $
