#use wml::debian::translation-check translation="d104e9d3d8d4ae3b889715564e3331db20e50728" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un problème de dépassement de tas a été détecté dans libebml, une
bibliothèque pour lire et écrire des fichiers au format EBML, un équivalent
binaire du format XML. Ces problèmes apparaissent dans plusieurs fonctions
ReadData de classes diverses de type de données. Cette mise à jour corrige aussi
le problème dans EbmlString::ReadData et EbmlUnicodeString::ReadData qui était
mentionné dans
<a href="https://security-tracker.debian.org/tracker/CVE-2021-3405">CVE-2021-3405</a>.</p>


<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.3.4-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libebml.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libebml, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libebml">\
https://security-tracker.debian.org/tracker/libebml</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2629.data"
# $Id: $
