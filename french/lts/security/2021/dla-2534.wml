#use wml::debian::translation-check translation="2b1523fcdf2789caa4eab5a91dfa160057f5a919" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le Qualys Research Labs a découvert une vulnérabilité de dépassement de tas
dans sudo, un programme conçu pour donner des droits limités de superutilisateur
à des utilisateurs particuliers. Tout utilisateur local (qu'il soit présent dans
le fichier sudoers ou non) peut exploiter ce défaut pour une élévation de
privilèges administrateur.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.8.19p1-2.1+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sudo.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sudo, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sudo">https://security-tracker.debian.org/tracker/sudo</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2534.data"
# $Id: $
