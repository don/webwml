#use wml::debian::translation-check translation="3bcb020add6b561fccb25a8843d5e007249e9f5e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
Deux vulnérabilité ont été découvertes dans python-gnupg, une enveloppe
Python pour GNU Privacy Guard.

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12020">CVE-2018-12020</a>

<p>Marcus Brinkmann a découvert que les versions de GnuPG antérieures
à 2.2.8 géraient incorrectement certains paramètres en ligne de commande.
Un attaquant distant pouvait utiliser cela pour usurper la sortie de GnuPG
et faire que des couriels non signés paraissent signés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6690">CVE-2019-6690</a>

<p>Il a été découvert que python-gnupg gérait incorrectement la phrase de
passe de GPG. Un attaquant distant pouvait envoyer une phrase de passe
contrefaite pour l'occasion qui pourrait lui permettre de contrôler la
sortie des opérations de chiffrement et de déchiffrement.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 0.3.9-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-gnupg.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python-gnupg,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python-gnupg">\
https://security-tracker.debian.org/tracker/python-gnupg</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2862.data"
# $Id: $
