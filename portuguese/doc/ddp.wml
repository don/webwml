#use wml::debian::ddp title="Projeto de documentação Debian"
#use wml::debian::translation-check translation="e8de3f9543c6c16d99765f9d2b60925bd6495dd0"

<p>O projeto de documentação Debian foi formado para coordenar e unificar
todos os esforços para escrever documentação de maior qualidade em maior
quantidade para o sistema Debian.</p>

  <h2>Tarefas do DDP</h2>
<div class="line">
  <div class="item col50">

    <h3>Manuais</h3>
    <ul>
      <li><strong><a href="user-manuals">Manuais de usuários(as)</a></strong></li>
      <li><strong><a href="devel-manuals">Manuais de desenvolvedores(as)</a></strong></li>
      <li><strong><a href="misc-manuals">Manuais diversos</a></strong></li>
      <li><strong><a href="#other">Manuais problemáticos</a></strong></li>
    </ul>

</div>

  <div class="item col50 lastcol">

    <h3>Política de documentação</h3>
    <ul>
      <li>Licença do manual compatível com a DFSG.</li>
      <li>Nós usamos o Docbook XML para nossos documentos.</li>
      <li>Os fontes devem ficar em <a href="https://salsa.debian.org/ddp-team">https://salsa.debian.org/ddp-team</a></li>
      <li><tt>www.debian.org/doc/&lt;manual-name&gt;</tt> será a URL oficial.</li>
      <li>Todo documento deve ser arquivado e mantido ativamente.</li>
      <li>Por favor, avise em <a href="https://lists.debian.org/debian-doc/">debian-doc</a>
se você for escrever um novo documento.</li>
    </ul>

    <h3>Acesso ao Git</h3>
    <ul>
      <li><a href="vcs">Como acessar</a> os repositórios do DDP no git.</li>
    </ul>

  </div>


</div>

<hr />

<h2><a name="other">Manuais problemáticos</a></h2>

<p>Além dos manuais normalmente anunciados, nós mantemos os manuais a
seguir, que são problemáticos de um jeito ou outro, então nós não podemos
recomendá-los a todos(as) os(as) usuários(as). Você foi alertado.</p>

<ul>
  <li><a href="obsolete#tutorial">Tutorial Debian</a>, obsoleto</li>
  <li><a href="obsolete#guide">Guia Debian</a>, obsoleto</li>
  <li><a href="obsolete#userref">Manual de referência do(a) usuário(a) Debian</a>,
      parado e bastante incompleto</li>
  <li><a href="obsolete#system">Manual dos(as) administradores(as) de sistemas Debian</a>,
parado, quase vazio</li>
  <li><a href="obsolete#network">Manual dos(as) administradores(as) de redes Debian</a>,
parado, incompleto</li>
  <li><a href="obsolete#swprod">Como os(as) produtores(as) de softwares podem distribuir
seus produtos diretamente no formato .deb</a>, parado, desatualizado</li>
  <li><a href="obsolete#packman">Manual de empacotamento Debian</a>, parcialmente
encontrado no <a href="devel-manuals#policy">Manual de políticas Debian</a>,
      o restante será incluído em um manual de referência do dpkg que ainda
está sendo escrito</li>
  <li><a href="obsolete#makeadeb">Introdução: criando um pacote Debian</a>,
tornado obsoleto pelo
      <a href="devel-manuals#maint-guide">Guia dos(as) novos(as) mantenedores(as) Debian</a></li>
  <li><a href="obsolete#programmers">Manual dos(as) programadores(as) Debian</a>,
      tornado obsoleto pelo
      <a href="devel-manuals#maint-guide">Guia dos(as) novos(as) mantenedores(as) Debian</a>
      e
      <a href="devel-manuals#debmake-doc">Guia dos(as) mantenedores(as) Debian</a></li>
  <li><a href="obsolete#repo">Repositório Debian HOWTO</a>, desatualizado após a
introdução do APT seguro</li>
  <li><a href="obsolete#i18n">Introdução ao i18n</a>, parado</li>
  <li><a href="obsolete#sgml-howto">Debian SGML/XML HOWTO</a>, parado, obsoleto</li>
  <li><a href="obsolete#markup">Manual de Marcação DebianDoc-SGML</a>,
parado; O DebianDoc está prestes a ser removido</li>
</ul>
