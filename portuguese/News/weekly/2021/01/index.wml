#use wml::debian::projectnews::header PUBDATE="2021-03-18" SUMMARY="Boas-vindas ao DPN, DPL, Freeze Softly, Secure GRUB2, Tema Homeworld, RC Bugs, atualização do site web, APT2.2, QEMU, mime types, rust-coreutils, debuginfod, Google e Debian, financiamento Freexian, novas assinaturas de chaves, BSPs e contribuidores(as)."
#use wml::debian::acronyms
#use wml::debian::translation-check translation="dc6b8f3e048c8f288c5efd1d612b75bbc4e51406"
# Status: [publihsed]

## substitute XXX with the number (expressed in letter) of the issue.
## please note that the var issue is not automagically localized, so
## translators need to put it directly in their language!
## example: <intro issue="fourth" />

## Use &#35; to escape # for IRC channels
## Use &#39 for single quotes

<shortintro issue="first"/>

<h2>Eleições para líder do Projeto Debian 2021</h2>

<p>A <a href="https://lists.debian.org/debian-devel-announce/2021/03/msg00001.html">
convocatória para candidaturas</a> foi anunciada para a eleição
do(a) <a href="https://www.debian.org/devel/leader">líder do Projeto Debian</a>.
O processo de eleição começa nas seis semanas anteriores &agrave;
vacância do posto, com o mandato iniciando em 21/04/2021. O processo segue da
seguinte forma: as candidaturas são aceitas de 7 a 13 de março, as campanhas
acontecem de 14 de março (domingo) até 3 de abril, e a votação começa em
4 de abril e vai até 17 de abril.</p>

<h2>Soft Freeze do Bullseye</h2>

<p>
A equipe de lançamento anunciou que
<a href="https://lists.debian.org/debian-devel-announce/2021/02/msg00002.html
">o Bullseye alcançou a etapa de soft freeze (congelamento suave) em 12/02/2021</a>.
Este congelamento somente permite correções específicas e pequenas para a
próxima versão. Novas transições ou versões de pacotes que podem ser
disruptivas não são mais permitidas. Você pode seguir a programação de
congelamento na página
<a href="https://release.debian.org/bullseye/freeze_policy.html">políticas e
linha do tempo do congelamento do Bullseye</a>.
</p>

<introtoc/>

<toc-display/>

<toc-add-entry name="security">Alertas de segurança importantes do Debian</toc-add-entry>

<p>A equipe de segurança do Debian emite alertas diariamente
(<a href="$(HOME)/security/2021/">alertas de segurança 2021</a>). Por favor,
leia-os cuidadosamente e se inscreva na
<a href="https://lists.debian.org/debian-security-announce/">lista de discussão de segurança</a>
para manter seus sistemas atualizados contra quaisquer vulnerabilidades.</p>

## Pull the below data directly from $(HOME)/security/2021/. Swap out the
## dsa-XXXX for the current advisory # , be sure to change the name as well.

<p>Alguns alertas recentemente lançados dizem respeito a esses pacotes:
<a href="$(HOME)/security/2021/dsa-4870">pygments</a>,
<a href="$(HOME)/security/2021/dsa-4869">tiff</a>,
<a href="$(HOME)/security/2021/dsa-4868">flatpack</a>,
<a href="$(HOME)/security/2021/dsa-4867">grub2</a>,
<a href="$(HOME)/security/2021/dsa-4866">thunderbird</a>,
<a href="$(HOME)/security/2021/dsa-4865">docker.io</a>,
<a href="$(HOME)/security/2021/dsa-4862">firefox</a>,
<a href="$(HOME)/security/2021/dsa-4861">screen</a>,
<a href="$(HOME)/security/2021/dsa-4858">chromium</a>,
<a href="$(HOME)/security/2021/dsa-4854">webkit2gtk</a>,
<a href="$(HOME)/security/2021/dsa-4852">openvswitch</a>,
<a href="$(HOME)/security/2021/dsa-4849">firejail</a> e
<a href="$(HOME)/security/2021/dsa-4847">connman</a>.
</p>

<p>O site web do Debian também <a href="https://www.debian.org/lts/security/">arquiva</a>
os avisos de segurança comunicados pela equipe de suporte de longo prazo
Debian (LTS) e enviados para a
<a href="https://lists.debian.org/debian-lts-announce/">lista de discussão debian-lts-announce</a>.
</p>

<toc-add-entry name="secureboot">Assegurando o SecureBoot GRUB2 UEFI 2021</toc-add-entry>

<p>Desde o conjunto de bugs <a href="https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot">BootHole</a>
anunciados no GRUB2 em julho de 2020, pesquisadores(as) de segurança e
desenvolvedores(as) no Debian e em outros lugares continuaram procurando por
mais problemas que poderiam permitir a evasão do Boot Seguro UEFI. Muitos outros
problemas fora encontrados. Veja o <a href="https://www.debian.org/security/2021/dsa-4867">aviso de segurança Debian 4867-1</a>
para mais detalhes completos. O Debian publicou uma
<a href="https://www.debian.org/security/2021-GRUB-UEFI-SecureBoot/">declaração</a>
muito informativa buscando explicar as consequências dessas
vulnerabilidades de segurança e os passos a serem tomados para corrigi-las.</p>

<toc-add-entry name="bullseye">Notícias do Debian <q>bullseye</q></toc-add-entry>
## The first sub section is dedicated to news about bullseye in order to
## build anticipation for our $Next_Release.

## As we get closer to the release date, this section will home the
## Release-Critical bugs report.

<p><b>Homeworld, a arte e tema padrão para o <q>bullseye</q></b></p>
<p>
Congratulamos à Juliette Taka por sua submissão vencedora
<a href="https://wiki.debian.org/DebianArt/Themes/Homeworld">Homeworld</a>,
a qual será o tema e a arte padrão para o Debian 11 <q>bullseye</q>. Mais de
5.613 votos foram enviados entre as <a
href="https://wiki.debian.org/DebianDesktop/Artwork/Bullseye">18
submissões</a>. Agradecemos a todos(as) que contribuíram e votaram em um
processo que foi uma agradável recordação de como o Debian é uma verdadeira
comunidade.
</p>

<p>Relatório de bugs críticos ao lançamento para a semana 11</p>

<p>A <a href="https://udd.debian.org/bugs.cgi">interface web de bugs do
Ultimate Debian Database</a>
apresenta, atualmente, os seguintes bugs críticos ao lançamento:</p>

<table>
  <tr><th>No total:</th><td><a href="https://udd.debian.org/bugs.cgi?release=any&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">1110</a></td></tr>
  <tr><th>Afetando Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">216</a></td></tr>
  <tr><th>Apenas Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_not_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">51</a></td></tr>
  <tr><th>Restando a ser corrigido no Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1"><b>165</b></a></td></tr>
</table>

<p>Desses <b><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">165</a></b> bugs, as seguintes tags estão definidas:</p>

<table>
  <tr><th>Pendente no Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=only&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">4</a></td></tr>
  <tr><th>Corrigidos no Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=only&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">23</a></td></tr>
  <tr><th>Duplicados no Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=only&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">10</a></td></tr>
  <tr><th>Podem ser corrigidos por uma atualização de segurança:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=only&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">15</a></td></tr>
  <tr><th>Contrib ou non-free no Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=only&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">4</a></td></tr>
  <tr><th>Reivindicados no Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=only&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">0</a></td></tr>
  <tr><th>Atrasados no Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=only&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">1</a></td></tr>
  <tr><th>De outra forma, corrigidos no Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=only&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">8</a></td></tr>
</table>

<p>Ignorando tudo acima (múltiplas tags possíveis) <a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=ign&amp;pending=ign&amp;security=ign&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=ign&amp;deferred=ign&amp;notmain=ign&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=ign&amp;done=ign&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1"><b>111</b></a>
bugs precisam ser corrigidos por contribuidores(as) Debian para se ter o
Debian 11 <q>Bullseye</q> lançado.</p>

<p>Contudo, na visão da gerência de lançamento,
<a href="https://udd.debian.org/bugs.cgi?release=bullseye&amp;patch=&amp;pending=&amp;security=ign&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=ign&amp;notbullseye=ign&amp;base=&amp;standard=&amp;merged=ign&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1"><b>182</b></a>
precisam de atenção para que o lançamento aconteça.</p>

<p>Por favor, veja <q><a
href="https://wiki.debian.org/ProjectNews/RC-Stats">Interpretando as
estatísticas de bugs críticos ao lançamento</a></q> para uma explicação dos
diferentes números.</p>



<toc-add-entry name="web">Novo design do site web</toc-add-entry>

<p>Estamos muito orgulhosos(as) de nossa equipe WWW e de seu trabalho na
<a href="https://bits.debian.org/2020/12/debian-web-new-homepage.html">atualização</a>
do <a href="https://www.debian.org">site web do Debian</a> para uma aparência
mais dinâmica e moderna. Dê uma olhada!
Isto é só o início do processo, já que continuamos removendo informações
obsoletas, atualizando o site com novas informações e melhorando a experiência
geral dos(as) usuários(as) finais. Como sempre, mais mãos e olhos serão úteis,
nos informe se desejar contribuir com este novo capítulo do nosso
desenvolvimento.</p>

<toc-add-entry name="apt">apt-2.2</toc-add-entry>

<p>Julian Andres Klode <a href="https://blog.jak-linux.org/2021/02/18/apt-2.2/">compartilhou</a>
que o APT 2.2 foi lançado. Novos recursos incluem <i>--error-on=any</i> e
<i>rred</i> independentes para combinar arquivos
pdiff.</p>


<toc-add-entry name="keys">Novas chaves de assinatura do repositório</toc-add-entry>

<p>As novas chaves de assinatura do repositório para o Debian 11 foram geradas
para uso futuro (em breve). As chaves serão incluídas no Debian 11,
<q>bullseye</q>, e para futuras versões pontuais para o Debian 10,
<q>buster</q>. As chaves entrarão em uso no lançamento do bullseye ou no
vencimento das chaves antigas em 12/04/2027.</p>

As novas chaves são:
<p>
<table>
<tr><th>pub</th>   <th>rsa4096 2021-01-17 [SC] [expires: 2029-01-15]</th></tr>
<tr><th>   </th>   <th>1F89 983E 0081 FDE0 18F3  CC96 73A4 F27B 8DD4 7936</th></tr>
<tr><th>uid</th>   <th>Debian Archive Automatic Signing Key (11/bullseye) &#60;ftpmaster@debian.org&#62;</th></tr>
<tr><th>sub</th>   <th> rsa4096 2021-01-17 [S] [expires: 2029-01-15]</th></tr>
<tr><th>   </th>   <th>A723 6886 F3CC CAAD 148A  27F8 0E98 404D 386F A1D9</th></tr>
</table>

<p>
<table>
<tr><th>pub</th>   <th>rsa4096 2021-01-17 [SC] [expires: 2029-01-15]</th></tr>
<tr><th>   </th>   <th>AC53 0D52 0F2F 3269 F5E9  8313 A484 4904 4AAD 5C5D</th></tr>
<tr><th>uid</th>   <th>Debian Security Archive Automatic Signing Key (11/bullseye) &#60;ftpmaster@debian.org&#62;</th></tr>
<tr><th>sub</th>   <th>rsa4096 2021-01-17 [S] [expires: 2029-01-15]</th></tr>
<tr><th>   </th>   <th>ED54 1312 A33F 1128 F10B  1C6C 5440 4762 BBB6 E853</th></tr>
</table>

<p>
<b>Chaves:</b>
<p><a href="https://ftp-master.debian.org/keys/archive-key-11.asc">https://ftp-master.debian.org/keys/archive-key-11.asc</a></p>
<p><a href="https://ftp-master.debian.org/keys/archive-key-11-security.asc">https://ftp-master.debian.org/keys/archive-key-11-security.asc</a></p>

<toc-add-entry name="dqib">Imagens pré-preparadas Debian Quick Image Baker estão disponíveis</toc-add-entry>
<p>DQIB (Debian Quick Image Baker)
fornece <a href="https://people.debian.org/~gio/dqib/">imagens Debian sid QEMU</a>,
geradas semanalmente e para muitas arquiteturas. Cada download fornece um
sistema de arquivos, kernel, initrd e uma amostra README de comandos QEMU que
iniciarão a imagem com informações sobre como fazer login.</p>

<toc-add-entry name="other">Outros itens de interesse</toc-add-entry>
<ul>
<li>Charles Plessy <a href="https://lists.debian.org/debian-devel/2021/01/msg00057.html">adicionou
centenas de tipos de mídia para /etc/mime.types</a>.</li>
<li>Nova lista de discussão: <a href="https://lists.debian.org/debian-localgroups/">debian-localgroups@lists.debian.org</a>
foi criada para facilitar a comunicação e prover suporte para eventos locais
e atividades locais.</li>
</ul>

<toc-add-entry name="rust">Debian rodando em coreutils do Rust</toc-add-entry>
<p>Sylvestre Ledru <a href="https://sylvestre.ledru.info/blog/2021/03/09/debian-running-on-rust-
coreutils">compartilhou detalhes</a> do funcionamento do <a href="https://tracker.debian.org/pkg/rust-coreutils">rust-coreutils</a> no
Debian.  A implementação é capaz de suportar e iniciar com GNOME, instalar os
1.000 principais pacotes e construir o Firefox.
</p>

<toc-add-entry name="debuginfod">Novo serviço: debuginfod</toc-add-entry>
<p>Sergio Durigan Junior anunciou um <a href="https://wiki.debian.org/Debuginfod">serviço debuginfod no Debian</a>.
O <a href="https://sourceware.org/elfutils/Debuginfod.html">debuginfod</a>
permite que desenvolvedores(as) não precisem instalar pacotes debuginfo
para depurar software. Ele funciona como um cliente/servidor para
fornecer ferramentas de depuração sobre HTTP.
</p>




####Here is where we move to EXTERNAL NEWS about Debian####
## News about Debian, a Debian influence in F/OSS, F/OSS, mentions of Debian
## in the news or other media.
<toc-add-entry name="bazel">Google e Debian colaboram para empacotar o sistema
de construção Bazel no Debian</toc-add-entry>

<p>
O desenvolvedor Debian Olek Wojnar e o engenheiro de software da Google Yun Peng
trabalharam com a <a href="https://blog.bazel.build/2021/03/04/bazel-debian-packaging.html">equipe
Bazel</a> para empacotar o <a href="https://opensourcelive.withgoogle.com/events/bazel/register?after-register
=%2Fevents%2Fbazel%2Fwatch%3Ftalk%3Ddebian-ubuntu-packaging">Bazel no
Debian</a> com a intenção de auxiliar a comunidade médica nas pesquisas sobre
COVID-19. Olek compartilhou alguns dos desafios técnicos e detalhes do projeto
em uma <a href="https://www.youtube.com/watch?v=jLSgky4ISj0&amp;t=23s">palestra em vídeo</a>.
</p>


<toc-add-entry name="freexian">Freexian provê financiamento para alguns projetos Debian</toc-add-entry>

<p>
Raphael Hertzog compartilhou detalhes da intenção do LTS do Freexian para
<a href="https://lists.debian.org/debian-project/2020/11/msg00002.html">fornecer financiamentos</a>
para alguns projetos Debian com parte do dinheiro coletado de seus(suas)
próprios(as) patrocinadores(as). Esta contribuição generosa permitirá que
equipes solicitem financimento dentro de suas esferas de trabalho que,
de modo geral, converterá para toda a comunidade.
</p>

<toc-add-entry name="events">BSPs, Eventos, MiniDebCamps e MiniDebConfs</toc-add-entry>

<p><b>Festas de caça aos bugs</b></p>

<p><b>Próximos eventos</b></p>

<p>
Vai acontecer uma
<a href="https://lists.debian.org/debian-devel-announce/2021/02/msg00000.html">festa virtual de caça aos bugs em Salzburg/Áustria</a>
entre 24&ndash;25 de abril de 2021.
Alguns detalhes ainda estão sendo planejados, por enquanto guardem esta
data.</p>

<p><b>Eventos passados</b></p>

<p>
A comunidade Debian Brasil organizou a
<a href="https://mdcobr2020.debian.net/">MiniDebConf On-line Brasil 2020</a>
entre 28&ndash;29 de novembro de 2020. Apresentações do evento em português estão
disponíveis para visualização em
<a href="https://peertube.debian.social/videos/watch/playlist/875b2409-2d73-4993-
8471-2923c27b8a7e">.</a>
</p>

<p>A comunidade Debian Índia organizou a
<a href="https://in2021.mini.debconf.org/">MiniDebConf On-line Índia 2021</a>
entre 23&ndash;24 de janeiro de 2021. As apresentações ocorreram em mais de 6
idiomas, num total de 45 atividades desde palestras, até "desconferências" (BOFs)
e oficinas. As palestras e os <a
href="https://meetings-archive.debian.net/pub/debian-meetings/2021/MiniDebConf-
India/">vídeos</a> do evento estão disponíveis para visualização.
</p>


<toc-add-entry name="reports">Relatórios</toc-add-entry>

## Team and progress reports. Consider placing reports from teams here rather
## than inside of the internal news sections.



## It's easier to link to the monthly reports for the LTS section and the RB links rather than
# summarize each report or number of packages. Feel free to input this information if you need to fill
# the issue out
#


<p><b>Relatórios mensais do Freexian LTS</b></p>

<p>Freexian lança <a href="https://raphaelhertzog.com/tag/Freexian+LTS/">relatórios mensais</a>
sobre o trabalho de contribuidores(as) pagos(as) para o Debian com suporte de
longo prazo (Long Term Support - LTS).
</p>

<p><b>Atualização do estado do Reproducible Builds</b></p>

<p>Siga o <a
href="https://reproducible-builds.org/blog/">blog do Reproducible
Builds</a>para obter relatórios mensais deste trabalho no ciclo <q>buster</q>.
</p>


<p><b>Pacotes precisando de ajuda:</b></p>

## link= link to the mail report from wnpp@debian.org to debian-devel ML
## orphaned= number of packages orphaned according to $link
## rfa= number of packages up for adoption according to $link

<wnpp link="https://lists.debian.org/debian-devel/2021/03/msg00063.html"
        orphaned="1204"
        rfa="209" />

<p><b>Bugs para novatos(as)</b></p>

## check https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer and add outstanding + forwarded + pending upload
<p>
O Debian tem uma tag de bug <q>newcomer</q> (novato(a)), usada para indicar
bugs que são adequados para novos(as) contribuidores(as) como ponto de entrada
para trabalhar com pacotes específicos. Atualmente existem
<a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer">189</a>
bugs disponíveis e com tag <q>newcomer</q>.
</p>


<toc-add-entry name="code">Código, programadores(as) e contribuidores(as)</toc-add-entry>
<p><b>Novos(as) mantenedores(as) de pacotes desde 9 de setembro de 2020</b></p>
##Run the ./new-package-maintainers script locally
##./new-package-maintainers YYYMMDD
<p>
Por favor, deem as boas-vindas a: Adrien Nayrat, Georgy Komarov, Alex Doyle,
Johann Queuniet, Stephen Gildea, Christoph Groth, Jhon Alejandro Marin
Rodriguez, Adrià García-Alzórriz, Romain Porte, Jakub Ružička, skyper, James
Turton, Alois Schlögl, Judit Foglszinger, Aaron Boxer, Kevin Wu, Anthony
Perkins, Felix Delattre, Ken Ibbotson, Andrei Rozanski, Nis Martensen,
qinxialei, Laurin Hagemann, Jai Flack, Johann Elsass, Fred Le Meur, Vivek K J,
Thiago da Silva Gracini, Jobin J, Selvamani Kannan, Calum McConnell, Dhyey
Patel, Ed Neville, Leonidas S. Barbosa, Lucca Braga Godoy Mendonça, Chris
Keller, Guinness, Sergio de Almeida Cipriano Junior, Sahil Dhiman, Michel Le
Bihan, Fabio Fantoni, Mark Pearson, Matija Nalis, David Bannon, Federico Grau,
Lisa Julia Nebel, Patrick Jaap, Francisco Emanoel Ferreira, Peymaneh Nejad,
Daniel Milde, Stefan Kropp, Frédéric Pierret, Vipul Kumar, Jarrah Gosbell,
John Zaitseff, Badreddin Aboubakr, Sam Reed, Scupake, Clay Stan, Klaumi
Klingsporn, Vincent Smeets, Emerson dos Santos Queiroz, Alexander Sulfrian,
bill-auger, Marcelo Henrique Cerri, Dan Streetman, Hu Feng, Andrea Righi,
Matthias Klein, Eric Brown, Mayco Souza Berghetti, Robbi Nespu, Simon Tatham e
Brian Potkin.
</p>

<p><b>Novos(as) mantenedores(as) Debian</b></p>
##Run the ./dd-dm-from-keyring.pl script locally for new DM and DD
##./dd-dm-from-keyring.pl YYYMMDD
<p>
Por favor, deem as boas-vindas a: Ricardo Ribalda Delgado, Pierre Gruet,
Henry-Nicolas Tourneur, Aloïs Micard, Jérôme Lebleu, Nis Martensen, Stephan
Lachnit, Felix Salfelder, Aleksey Kravchenko, Étienne Mollier, Timo Röhling,
Fabio Augusto De Muzio Tobich, Arun Kumar Pariyar, Francis Murtagh, William
Desportes, Robin Gustafsson, Nicholas Guriev, Xiang Gao, Maarten L. Hekkelman,
qinxialei, Boian Bonev, Filip Hroch e Antonio Valentino.
</p>

<p><b>Novos(as) desenvolvedores(as) Debian</b></p>
<p>
Por favor, deem as boas-vindas a: Benda XU, Joseph Nahmias, Marcos Fouces,
Hayashi Kentaro, James Valleroy, Helge Deller, Nicholas D Steeves, Nilesh Patra,
David Suárez Rodríguez e Pierre Gruet.
</p>

<p><b>Contribuidores(as)</b></p>
## Visit the link below and pull the information manually.

<p>
954 pessoas e 9 equipes estão atualmente listadas na página
<a href="https://contributors.debian.org/">Contribuidores(as) Debian</a> em
2021.
</p>

<p><b>Estatísticas</b></p>
##Pull this information from sources.d.o
##https://sources.debian.org/stats/sid/
##https://sources.debian.org/stats/buster/
<p><b><em>buster</em></b></p>
<ul style="list-style-type:none">
<li>Arquivos-fonte: 12.323.884</li>
<li>Pacotes-fonte: 28.925</li>
<li>Uso do disco: 264.071.084 kB</li>
<li>Ctags: 9.487.034</li>
<li>Linhas de código-fonte: 1.077.110.982</li>
</ul>

<p><b><em>sid</em></b></p>
<ul style="list-style-type:none">
<li>Arquivos-fonte: 16.868.320</li>
<li>Pacotes-fonte: 33.215</li>
<li>Uso do disco: 364.735.804 kB</li>
<li>Ctags: 3.343.666</li>
<li>Linhas de código-fonte: 1.510.195.519</li>
</ul>

<p><b>Pacotes populares</b></p>
##Taken via UDD query from popcon
<ul>
<li><a href="https://packages.debian.org/buster/tar">tar</a>: 99.100 usuários(as) diários</li>
<li><a href="https://packages.debian.org/buster/popularity-contest">popularity-contest</a>: 96.010 usuários(as) diários</li>
<li><a href="https://packages.debian.org/buster/debianutils">debianutils</a>: 112.659 instalações</li>
<li><a href="https://packages.debian.org/buster/gparted">gparted</a>: 14.909 instalações</li>
<li><a href="https://packages.debian.org/buster/grub2-common">grub2-common</a> 54.040 atualizações recentes</li>
</ul>

<p><b>Pacotes novos e de destaque</b></p>

<p>
Aqui está uma pequena amostra dos muitos pacotes <a href="https://packages.debian.org/unstable/main/newpkg">
adicionados ao repositório Debian instável (unstable)</a> nas últimas
semanas:</p>

<ul>
<li><a href="https://packages.debian.org/unstable/main/dnsperf">dnsperf - ferramenta de teste de desempenho de DNS</a></li>
<li><a href="https://packages.debian.org/unstable/main/ptpython">ptpython -prompt alternativo de Python com autocompletação</a></li>
<li><a href="https://packages.debian.org/unstable/main/science-datamanagement">science-datamanagement - pacotes Debian de gerenciamento de dados científicos</a></li>
<li><a href="https://packages.debian.org/unstable/main/dnscap">dnscap - utilitário de captura de rede projetado especificamente para tráfego DNS</a></li>
<li><a href="https://packages.debian.org/unstable/main/logiops">logiops - utilitário de configuração para mouses e teclados Logitech</a></li>
<li><a href="https://packages.debian.org/unstable/main/node-cron-validator">node-cron-validator -- cron-validator é um utilitário que permite a validação de expressões cron</a></li>
</ul>

<toc-add-entry name="discuss">Discussões</toc-add-entry>

<p>O desenvolvedor Debian Stephan Lachnit perguntou: <a href="https://lists.debian.org/debian-devel/2021/02/msg00282.html"><i>É
possível contribuir com o Debian sem usar o nome verdadeiro devido a preocupações com privacidade</i></a>?".</p>

<p>William Torrez Corea perguntou: <a href="https://lists.debian.org/debian-user/2021/01/msg00321.html"><i>Como
atualizar o SO para Debian buster</i></a>? A discussão se relaciona à
reinstalação vs. atualização vs. reconstrução, e uma síntese das versões
Sid/estável (stable)/teste (testing).
</p>

<p>Jerry Mellon perguntou: <a  href="https://lists.debian.org/debian-user/2021/01/msg00584.html"><i>Como adicionar
um disco rígido a um sistema existente</i></a>? Fácil de ler, porém apresenta
algumas armadilhas e alternativas para esta tarefa muito comum.
</p>

<p>Dan Hitt perguntou: como <a
href="https://lists.debian.org/debian-user/2021/01/msg00746.html"><i>instalar
o Debian 10 sem acesso a CD ou USB, mas com uso de ethernet e disco
rígido</i></a>? A discussão cobre uma bios muito rápida, opções pxeboot, entradas
grub menuentry, netboot e uma solução kernel hd-media.</p>

<p>John Berden perguntou: <a href="https://lists.debian.org/debian-user/2021/02/msg00164.html"><i>Como corrigir
uma senha incorreta no Debian 10.8 após a instalação</i></a>?. Esta discussão
se refere à edição do grub na inicialização, persistência grub, um emacs em
miniatura e uma lição de sintaxe do editor grub, e comportamento canônico do
shell.</p>

<p><b>Dicas e truques</b></p>

<ul>
<li>Craig Small explica o <a href="https://dropbear.xyz/2021/01/18/percent-cpu-for-processes/">campo
CPU/pcpu do programa ps</a>, a maioria de nós o executa como ps aux. Aqui está
uma ótima explicação de como os cálculos são ordenados e apresentados.</li>

<li>Vincent Fourmond compartilha todas as suas <a href="https://vince-debian.blogspot.com/2021/03/all-tips-and-tricks-about-qsoas.
html">dicas e truques sobre QSoas</a>.</li>

<li>Bastian Venthur detalha <a href="https://venthur.de/2021-02-10-installing-debian-on-a-thinkpad-t14s.
html">instalando o Debian em um Thinkpad T14s</a>.</li>
</ul>

<p><b>Era uma vez no Debian:</b></p>

## Items pulled from the Timeline https://timeline.debian.net
## Jump to any random year/ same month/ same week.
## Provide link and link description.
## This may work better with a script at some point, but for now let us see
## what the ease of work is.

## Format - YYYY-MM-DD text

<ul>
<li>13/03/2010 até 19/03/2010<a href="https://wiki.debian.org/DebianThailand/MiniDebCamp2010">MiniDebCamp Tailândia 2010</a></li>

<li>15/03/2014&ndash;16/03/2014 <a href="https://wiki.debian.org/DebianWomen/Projects/MiniDebconf-Women/2014/">Women MiniDebConf, Barcelona, Espanha</a></li>

<li>16/03/2000 <a href="https://www.debian.org/vote/2000/vote_0007">Wichert Akkerman reeleito como DPL</a></li>

<li>17/03/1997 <a href="https://www.debian.org/News/1997/19970317">Ian Murdock eleito como presidente do Conselho de Diretores(as)</a></li>

<li>17/03/2005 <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=300000">Bug do Debian #300000 reportado por Florian Zumbiehl</a></li>

<li>17/03/2008 <a href="https://www.debian.org/devel/debian-installer/News/2008/20080317">Primeiro beta do debian-installer do lenny lançado</a></li>
</ul>

<toc-add-entry name="continuedpn">Quer continuar a ler o DPN?</toc-add-entry>
<continue-dpn />

<p><a href="https://lists.debian.org/debian-news/">Inscreva-se ou remova sua inscrição</a> da lista de discussão das Notícias do Debian</p>

#use wml::debian::projectnews::footer translator="Thiago Pezzo (tico), Paulo Henrique de Lima Santana (phls)" editor="The Publicity Team with contributions from Jean-Pierre Giraud, Justin B Rye, Thiago Pezzo, Paulo Henrique de Lima Santana (phls), Donald Norwood"
# No need for the word 'and' for the last contributor, it is added at build time.
# Please add the contributors to the /dpn/CREDITS file
# Translators may also add a translator="foo, bar, baz" to the previous line
