<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a possible signature verification issue in
firmware update daemon library fwupd as the return value <tt>of
gpgme_op_verify_result</tt> was not being checked</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10759">CVE-2020-10759</a>

    <p>Possible bypass in signature verification</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
0.7.4-2+deb9u1.</p>

<p>We recommend that you upgrade your fwupd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2274.data"
# $Id: $
