<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Thomas Chauchefoin from SonarSource discovered that in Zabbix, a
server/client network monitoring system, after the initial setup
process, some steps of setup.php file are reachable not only by
super-administrators, but by unauthenticated users as well. An
attacker could bypass checks and potentially change the configuration
of Zabbix Frontend.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1:3.0.32+dfsg-0+deb9u2.</p>

<p>We recommend that you upgrade your zabbix packages.</p>

<p>For the detailed security status of zabbix please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/zabbix">https://security-tracker.debian.org/tracker/zabbix</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2914.data"
# $Id: $
