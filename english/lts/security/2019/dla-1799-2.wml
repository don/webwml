<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<p>This updated advisory text adds a note about the need to install new
binary packages.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5995">CVE-2018-5995</a>

    <p>ADLab of VenusTech discovered that the kernel logged the virtual
    addresses assigned to per-CPU data, which could make it easier to
    exploit other vulnerabilities.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12126">CVE-2018-12126</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-12127">CVE-2018-12127</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-12130">CVE-2018-12130</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-11091">CVE-2019-11091</a>

    <p>Multiple researchers have discovered vulnerabilities in the way
    that Intel processor designs implement speculative forwarding of
    data filled into temporary microarchitectural structures
    (buffers).  This flaw could allow an attacker controlling an
    unprivileged process to read sensitive information, including from
    the kernel and all other processes running on the system, or
    across guest/host boundaries to read host memory.</p>

    <p>See <a href="https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/mds.html">https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/mds.html</a>
    for more details.</p>

    <p>To fully resolve these vulnerabilities it is also necessary to
    install updated CPU microcode.  An updated intel-microcode package
    (only available in Debian non-free) was provided via DLA-1789-1.
    The updated CPU microcode may also be available as part of a
    system firmware ("BIOS") update.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-2024">CVE-2019-2024</a>

    <p>A use-after-free bug was discovered in the em28xx video capture
    driver.  Local users might be able to use this for denial of
    service (memory corruption or crash) or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3459">CVE-2019-3459</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-3460">CVE-2019-3460</a>

    <p>Shlomi Oberman, Yuli Shapiro, and Karamba Security Ltd. research
    team discovered missing range checks in the Bluetooth L2CAP
    implementation.  If Bluetooth is enabled, a nearby attacker
    could use these to read sensitive information from the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3882">CVE-2019-3882</a>

    <p>It was found that the vfio implementation did not limit the number
    of DMA mappings to device memory.  A local user granted ownership
    of a vfio device could use this to cause a denial of service
    (out-of-memory condition).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3901">CVE-2019-3901</a>

    <p>Jann Horn of Google reported a race condition that would allow a
    local user to read performance events from a task after it
    executes a setuid program.  This could leak sensitive information
    processed by setuid programs.  Debian's kernel configuration does
    not allow unprivileged users to access peformance events by
    default, which fully mitigates this issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6133">CVE-2019-6133</a>

    <p>Jann Horn of Google found that Policykit's authentication check
    could be bypassed by a local user creating a process with the same
    start time and process ID as an older authenticated process.
    PolicyKit was already updated to fix this in DLA-1644-1.  The
    kernel has additionally been updated to avoid a delay between
    assigning start time and process ID, which should make the attack
    impractical.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9503">CVE-2019-9503</a>

    <p>Hugues Anguelkov and others at Quarkslab discovered that the
    brcmfmac (Broadcom wifi FullMAC) driver did not correctly
    distinguish messages sent by the wifi firmware from other packets.
    An attacker using the same wifi network could use this for denial
    of service or to exploit other vulnerabilities in the driver.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11190">CVE-2019-11190</a>

    <p>Robert Święcki reported that when a setuid program was executed it
    was still possible to read performance events while the kernel set
    up the program's address space.  A local user could use this to
    defeat ASLR in a setuid program, making it easier to exploit other
    vulnerabilities in the program.  Debian's kernel configuration
    does not allow unprivileged users to access peformance events by
    default, which fully mitigates this issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11486">CVE-2019-11486</a>

    <p>Jann Horn of Google reported numerous race conditions in the
    Siemens R3964 line discipline.  A local user could use these to
    cause unspecified security impact.  This module has therefore been
    disabled.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11599">CVE-2019-11599</a>

    <p>Jann Horn of Google reported a race condition in the core dump
    implementation which could lead to a use-after-free.  A local
    user could use this to read sensitive information, to cause a
    denial of service (memory corruption), or for privilege
    escalation.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.16.68-1.  This version also includes a fix for Debian bug #927781,
and other fixes included in upstream stable updates.</p>

<p>We recommend that you upgrade your linux and linux-latest
packages.  You will need to use "apt-get upgrade --with-new-pkgs"
or <q>apt upgrade</q> as the binary package names have changed.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1799-2.data"
# $Id: $
