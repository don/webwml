<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there was an arbitrary command execution
vulnerability in the Go programming language.</p>

<p>The <q>go get</q> implementation did not correctly validate <q>import path</q>
statements for "://" which allowed remote attackers to execute arbitrary
OS commands via a crafted web site.</p>

<p>For Debian 7 <q>Wheezy</q>, this issue has been fixed in golang version
2:1.0.2-1.1+deb7u3.</p>

<p>We recommend that you upgrade your golang packages. The Debian LTS team
would like to thank Abhijith PA for preparing this update.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1294.data"
# $Id: $
