<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities affecting the cups printing server were found
which can lead to arbitrary IPP command execution and denial of
service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18190">CVE-2017-18190</a>

    <p>A localhost.localdomain whitelist entry in valid_host() in
    scheduler/client.c in CUPS before 2.2.2 allows remote attackers to
    execute arbitrary IPP commands by sending POST requests to the
    CUPS daemon in conjunction with DNS rebinding. The
    localhost.localdomain name is often resolved via a DNS server
    (neither the OS nor the web browser is responsible for ensuring
    that localhost.localdomain is 127.0.0.1).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18248">CVE-2017-18248</a>

    <p>The add_job function in scheduler/ipp.c in CUPS before 2.2.6, when
    D-Bus support is enabled, can be crashed by remote attackers by
    sending print jobs with an invalid username, related to a D-Bus
    notification.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.7.5-11+deb8u3.</p>

<p>We recommend that you upgrade your cups packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1412.data"
# $Id: $
