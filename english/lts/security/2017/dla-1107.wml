<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-2099">CVE-2013-2099</a>

    <p>Bazaar bundles SSL certificate checking code from Python, which
    had a bug that could cause a denial of service via resource
    consumption through multiple wildcards in certificate hostnames.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14176">CVE-2017-14176</a>

    <p>Adam Collard found that host names in 'bzr+ssh' URLs were not
    parsed correctly by Bazaar, allowing remote attackers to run
    arbitrary code by tricking a user into a maliciously crafted
    URL.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.6.0~bzr6526-1+deb7u1.</p>

<p>We recommend that you upgrade your bzr packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1107.data"
# $Id: $
