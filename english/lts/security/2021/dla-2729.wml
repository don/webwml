<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue in the Asterisk telephony system.
If the IAX2 channel driver received a packet that contained an unsupported
media format, a crash could have occured.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32558">CVE-2021-32558</a>

	<p>An issue was discovered in Sangoma Asterisk 13.x before 13.38.3,
	16.x before 16.19.1, 17.x before 17.9.4, and 18.x before 18.5.1, and
	Certified Asterisk before 16.8-cert10. If the IAX2 channel driver
	receives a packet that contains an unsupported media format, a crash
	can occur.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1:13.14.1~dfsg-2+deb9u5.</p>

<p>We recommend that you upgrade your asterisk packages.</p>

<p>In addition, Asterisk are tracking this issue as <tt>AST-2021-008</tt></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2729.data"
# $Id: $
