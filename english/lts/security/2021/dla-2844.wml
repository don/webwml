<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Artem Ivanov and Joshua Rogers found an XSS and a DOS issue,
respectively, affecting src:privoxy, a non-caching web proxy with
advanced filtering capabilities for enhancing privacy, modifying
web page data and HTTP headers, controlling access, and removing
ads and other obnoxious Internet junk.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
3.0.26-3+deb9u3.</p>

<p>We recommend that you upgrade your privoxy packages.</p>

<p>For the detailed security status of privoxy please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/privoxy">https://security-tracker.debian.org/tracker/privoxy</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2844.data"
# $Id: $
