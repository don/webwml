<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the previous upload of the package
rabbitmq-server versioned 3.6.6-1+deb9u1 introduced a regression in
function fmt_strip_tags. Big thanks to Christoph Haas for the
reporting an issue and for testing the update.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.6.6-1+deb9u2.</p>

<p>We recommend that you upgrade your rabbitmq-server packages.</p>

<p>For the detailed security status of rabbitmq-server please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/rabbitmq-server">https://security-tracker.debian.org/tracker/rabbitmq-server</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2710-2.data"
# $Id: $
