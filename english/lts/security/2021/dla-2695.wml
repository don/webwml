<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in klibc.  Depending on
how klibc is used, these could lead to the execution of arbitrary
code, privilege escalation, or denial of service.</p>

<p>Thanks to Microsoft Vulnerability Research for reporting the heap bugs
and going some of the way to identifying the cpio bugs.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31870">CVE-2021-31870</a>

    <p>Multiplication in the calloc() function may result in an integer
    overflow and a subsequent heap buffer overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31871">CVE-2021-31871</a>

    <p>An integer overflow in the cpio command may result in a NULL
    pointer dereference.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31872">CVE-2021-31872</a>

    <p>Multiple possible integer overflows in the cpio command on 32-bit
    systems may result in a buffer overflow or other security impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31873">CVE-2021-31873</a>

    <p>Additions in malloc() function may result in integer overflow and
    subsequent heap buffer overflow.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.0.4-9+deb9u1.</p>

<p>We recommend that you upgrade your klibc packages.</p>

<p>For the detailed security status of klibc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/klibc">https://security-tracker.debian.org/tracker/klibc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2695.data"
# $Id: $
