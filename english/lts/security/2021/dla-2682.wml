<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>mrxvt, lightweight multi-tabbed X terminal emulator, allowed
(potentially remote) code execution because of improper handling
of certain escape sequences (ESC G Q).</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.5.4-2+deb9u1.</p>

<p>We recommend that you upgrade your mrxvt packages.</p>

<p>For the detailed security status of mrxvt please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mrxvt">https://security-tracker.debian.org/tracker/mrxvt</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2682.data"
# $Id: $
