#use wml::debian::translation-check translation="4f5ef50b5d7008577cb1752c366cedd8f01980e3" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera sårbarheter har upptäckts i Linuxkärnan som
kan leda till utökning av privilegier, överbelastning eller
informationsläckage.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8553">CVE-2015-8553</a>

    <p>Jan Beulich upptäckte att <a href="https://security-tracker.debian.org/tracker/CVE-2015-2150">CVE-2015-2150</a> inte adresserades
    fullständigt. Om en fysisk PCI-funktion skickas igenom till en
    Xen-gäst kan gästen få åtkomst till dess minne och I/O-regioner
    innan avkodning av dessa regioner är aktiverat. Detta kunde resultera
    i överbelastning (oväntad NMI) på värden.</p>
    
    <p>Rättningen för detta är inkompatibel med qemu-versioner tidigare än 2.5.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18509">CVE-2017-18509</a>

    <p>Denis Andzakovic rapporterade en saknad typkontroll i implementationen 
    av IPv4 multicast routing. En användare med CAP_NET_ADMIN-möjligheten
    (i vilken användarnamnrymd som helst) kunde använda detta för
    överbelastning (minneskorruption eller krasch) eller möjligen för
    utökning av rättigheter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5995">CVE-2018-5995</a>

    <p>ADLab från VenusTech upptäckte att kärnan loggade de virtuella
    adresser som tilldelades till per-CPU-data, vilket kunde göra det
    lättare att exploatera andra sårbarheter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20836">CVE-2018-20836</a>

    <p>chenxiang rapporterade en kapplöpningseffekt i libsas,
    kärnundersystemet som ger stöd för Serial Attached SCSI (SAS)-enheter,
    vilket kunde leda till användning efter frigörning. Det är oklart
    hur detta kunde exploateras.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20856">CVE-2018-20856</a>

    <p>Xiao Jin rapporterade en potentiell dubbel frigörning i
    blockundersystemet, om ett fel inträffar vid initiering av
    I/O-schemaläggaren för en blockenhet. Det är oklart hur detta kunde
    exploateras.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1125">CVE-2019-1125</a>

    <p>Man har upptäckt att de flesta x86-processorer spekulativt kunde
    hoppa över en villkorlig SWAPGS-instruktion som används när man
    går in i kärnan från user-mode, och/eller kunde spekulativt
    exekvera den när den skulle hoppas över. Detta är en undertyp av
    Spectre variant 1, vilket kunde tillåta lokala användare att få
    tillgång till känslig information från kärnan eller andra
    processer. Den har lindrats genom att använda minnesbarriärer för
    att begränsa spekulativ exekvering. System som använder en
    i386-kärna påverkas inte eftersom kärnan inte använder SWAPGS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3882">CVE-2019-3882</a>

    <p>Det har upptäckts att vfio-implementationen inte begränsar antalet
    DMA-mappningar till enhetsminne. En lokal användare som tilldelas
    ägarskap av en vfio-enhet kunde använda detta för att orsaka en
    överbelastning (slut-på-minne).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3900">CVE-2019-3900</a>

    <p>Man har upptäckt att vhost-drivrutinerna inte kontrollerar
    mängden arbete som utförs till tjänstförfråningar från gäst-VMs
    ordentligt. En illasinnad gäst kunde använda detta för att orsaka en
    överbelastning (obegränsat CPU-användning) på värden.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10207">CVE-2019-10207</a>

    <p>Verktyget syzkaller upptäckte en potentiell null-dereferens i
    olika drivrutiner för UART-anslutna Bluetooth-enheter. En lokal
    användare med åtkomst till en PTY-enhet eller andra lämpliga
    tty-enheter kunde använda detta för en överbelastning.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10638">CVE-2019-10638</a>

    <p>Amit Klein och Benny Pinkas upptäckte att generationen av IP-paket-IDs
    använde en svag hash-funktion, <q>jhash</q>. Detta kunde tillåta
    spårning av individuella datorer när de kommunicerar med olika
    fjärrservrar och från olika nätverk. Funktionen <q>siphash</q>
    används nu istället.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10639">CVE-2019-10639</a>

    <p>Amit Klein och Benny Pinkas upptäckte att generationen av IP-paket-IDs
    använde en svag hash-funktion som inkorporerade en virtuell
    kärnadress. Denna hash-funktion används inte längre för IP IDs,
    även om den fortfarande används för andra ändamål i nätverksstacken.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13631">CVE-2019-13631</a>

    <p>Man har upptäckt att drivrutinen gtco för USB-indataplattor kunde
    spilla över en stackbuffer med konstant data under tolkning av
    enhetens beskrivare. En fysiskt närvarande användare med en
    speciellt skapad USB-enhet kunde utnyttja detta för att orsaka
    en överbelastning, eller möjligen för utökning av privilegier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13648">CVE-2019-13648</a>

    <p>Praveen Pandey rapporterade att på PowerPC (ppc64el)-system utan
    Transactional Memory (TM), kom kärnan att ändå försöka att
    återställa TM-läge som skickas till systemanropet sigreturn(). En
    lokal användare kunde utnyttja detta för överbelastning.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14283">CVE-2019-14283</a>

    <p>Verktyget syzkaller upptäckte en saknad kontroll av gränser i
    floppydiskdrivrutinen. En lokal användare med tillgång till en
    floppydiskenhet med en disk instoppad kunde utnyttja detta för att läsa
    kärnminne bortom I/O-buffern, och därmed möjligen få åtkomst till
    känslig information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14284">CVE-2019-14284</a>

    <p>Verktyget syzkaller upptäckte en potentiell division med noll
    i floppydrivrutinen. En lokal användare med tillgång till en
    floppyenhet kunde använda detta för överbelastning.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15239">CVE-2019-15239</a>

    <p>Denis Andzakovic rapporterade en möjlig användning efter frigörning
    i TCP-socketsimplementationen. En lokal användare kunde använda
    detta för överbelastning (minneskorruption eller krasch) eller
    möjligen för utökning av privilegier.</p></li>

<li>(CVE ID inte tilldelat)

    <p>Undersystemet netfilter conntrack använde kärnadresser som
    användar-synliga IDs, vilket kunde göra det lättare att exploatera
    andra sårbarheter.</p></li>

<li>XSA-300

    <p>Julien Grall rapporterade att Linux inte begränsar mängden minne
    som en domän kommer att försöka att utnyttja, och inte heller
    mängden av "främmande / grant map" -minne som alla individuella
    gäster kan konsumera, vilket leder till överbelastning
    (för värd eller gäster).</p></li>

</ul>

<p>För den gamla stabila utgåvan (Stretch) har dessa problem rättats
i version 4.9.168-1+deb9u5.</p>

<p>För den stabila utgåvan (Buster), rättades dessa problem mestadels
i version 4.19.37-5+deb10u2 eller tidigare.</p>

<p>Vi rekommenderar att ni uppgraderar era linux-paket.</p>

<p>För detaljerad säkerhetsstatus om linux vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4497.data"
