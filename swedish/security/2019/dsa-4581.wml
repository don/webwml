#use wml::debian::translation-check translation="70be9894f7f65be4520c817dfd389d2ee7c87f04" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera sårbarheter har upptäckts i git, ett snabbt, skalbart,
distribuerat versionshanteringssystem.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1348">CVE-2019-1348</a>

    <p>Det har rapporterats att alternativet --export-marks i git fast-import
    även exponeras via in-stream-kommando-funktionaliteten export-marks=...,
    vilket tillåter att skriva över godtyckliga sökvägar.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1387">CVE-2019-1387</a>

    <p>Man har upptäckt att submodule-namn inte valideras strikt nog,
    vilket tilltåter riktade angrepp via fjärrkodsexekvering när man
    utför rekursiva kloner.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19604">CVE-2019-19604</a>

    <p>Joern Schneeweisz rapporterade en sårbarhet där en rekursiv klon
    följt av en submodule-uppdatering kunde exekvera kod som ingår i
    förrådet utan att användaren explicit har efterfrågat detta. Det tillåts
    inte längre att `.gitmodules` har inlägg som sätter
    `submodule.&lt;name&gt;.update=!command`.</p></li>

</ul>

<p>Utöver detta adresserar denna uppdatering ett antal säkerhetsproblem som
endast är ett problem om git verkar på ett NTFS-filsystem (<a href="https://security-tracker.debian.org/tracker/CVE-2019-1349">CVE-2019-1349</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-1352">CVE-2019-1352</a> och <a href="https://security-tracker.debian.org/tracker/CVE-2019-1353">CVE-2019-1353</a>).</p>

<p>För den gamla stabila utgåvan (Stretch) har dessa problem rättats
i version 1:2.11.0-3+deb9u5.</p>

<p>För den stabila utgåvan (Buster) har dessa problem rättats i
version 1:2.20.1-2+deb10u1.</p>

<p>Vi rekommenderar att ni uppgraderar era git-paket.</p>

<p>För detaljerad säkerhetsstatus om git vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/git">https://security-tracker.debian.org/tracker/git</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4581.data"
