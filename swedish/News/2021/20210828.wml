#use wml::debian::translation-check translation="eebf73f6f1306851590138e0b9fc3917cf4580a6"
<define-tag pagetitle>DebConf21 online avslutas</define-tag>

<define-tag release_date>2021-08-28</define-tag>
#use wml::debian::news

<p>
Lördagen den 28 augusti 2021 avslutades den årliga Debian utvecklar- och
bidragslämnarkonferensen.
</p>

<p>
DebConf21 har hållits online för andra gången på grund av Coronaviruspandemin
(COVID-19).
</p>

<p>
Alla sessioner har strömmats, med ett antal sätt att delta:
via IRC-möten, kollaborativa textdokument online,
och mötesrum för videokonferenser.
</p>

<p>
Med 740 registrerade deltagare från mer än 15 länder och totalt över
70 evenemangstal, diskussionssessioner, Birds of a Feather-möten (BoF) och
andra aktiviteter, var <a href="https://debconf21.debconf.org">DebConf21</a>
mycket framgångsrikt.
</p>

<p>
Upplägget som har gjorts för tidigare evenemang som inkluderar Jitsi, OBS,
Voctomix, SReview, nginx, Etherpad, en webbaserad frontend för
voctomix har förbättrats och har framgångsrikt använts för DebConf21.
Alla komponenter i videoinfrastrukturen är fri mjukvara, och konfigureras
genom videogruppens publika <a href="https://salsa.debian.org/debconf-video-team/ansible">ansibleförråd</a>.
</p>

<p>
DebConf21s <a href="https://debconf21.debconf.org/schedule/">schema</a> inkluderade
en stor variation av evenemang, grupperade i flera spår:
</p>
<ul>
<li>Introduktion till fri mjukvara &amp; Debian,</li>
<li>Paketering, policy och Debianinfrastruktur,</li>
<li>Systemadministration, automatisering och instrumentation,</li>
<li>Moln och containers,</li>
<li>Säkerhet,</li>
<li>Gemenskap, mångfald, lokalt uppsökande verksamhet och socialt sammanhang,</li>
<li>Internationalisering, lokalisering och tillgänglighet,</li>
<li>Inbäddade system &amp; kärnan,</li>
<li>Debian Blends och distributioner baserade på Debian,</li>
<li>Debian inom konst och vetenskap</li>
<li>och ytterligare andra.</li>
</ul>
<p>
Föreläsningarna har strömmats i två rum, och flera av dessa aktiviteter har
hållits i flera språk: telugu, portugisiska, malayalam, kannada, hindi,
marathi och engelska, vilket tillåter en mer mångsidig publik att avnjuta och
deltaga.
</p>

<p>
Mellan föreläsningarna har videoströmmen visat dom vanliga sponsorerna i
loopen, men även några ytterligare filmklipp inklusive fotografier från
tidigare DebConfs, roliga fakta om Debian och korta shout-out-videos som
har skickats in av deltagare för att kommunicera med sina Debianvänner.
</p>

<p>Debians publicitetsgrupp genomförde den vanliga "livetäckningen" för att
uppmuntra deltagande med mikronyheter tillkännagivande de olika evenemangen.
DebConfgruppen tillhandahöll även flera 
<a href="https://debconf21.debconf.org/schedule/mobile/">mobila alternativ
för att följa schemat</a>.
</p>

<p>
För dom som inte hade möjlighet att delta finns de flesta föreläsningar och
sessioner tillgängliga genom 
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2021/DebConf21/">\
webbsidan för Debianmöten</a> och de återstående kommer att dyka upp inom
de närmsta dagarna.
</p>

<p>
Webbplatsen för <a href="https://debconf21.debconf.org/">DebConf21</a>
kommer att hållas aktiv för arkivändamål och kommer att fortsätta
att erbjuda länkar till presentationerna och videorna av föreläsningarna
och evenemangen.
</p>

<p>
Nästa år planeras <a href="https://wiki.debian.org/DebConf/22">DebConf22</a> att
hållas i Prizren, Kosovo, i juli 2022.
</p>

<p>
DebConf arbetar för en säker och välkomnande miljö för alla deltagare.
Under konferensen finns flera grupper (Front desk, välkomnandegruppen och
gemenskapsgruppen) tillgängliga för att hjälpa till så att deltagare får
den bästa erfarenheten som är möjlig under konferensen, och hittar lösningar
på alla eventuella problem.
Se <a href="https://debconf21.debconf.org/about/coc/">webbsidan för
förhållningsregler på webbplatsen för DebConf21</a> för ytterligare detaljer
om detta.
</p>

<p>
Debian tackar för åtagandet från flera <a href="https://debconf21.debconf.org/sponsors/">sponsorer</a>
för deras stöd för DebConf21, speciellt våra platinasponsorer:
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://code4life.roche.com/">Roche</a>,
<a href="https://aws.amazon.com/">Amazon Web Services (AWS)</a>
och <a href="https://google.com/">Google</a>.
</p>

<h2>Om Debian</h2>
<p>
Debianprojektet grundades 1993 av Ian Murdock med målsättningen att vara
ett fullständigt fritt gemenskapsprojekt. Sedan dess har projektet
vuxit till ett av världens största och mest inflytelserika öppen
källkodsprojekt. Tusentals frivilliga från hela världen arbetar tillsammans
för att skapa och underhålla Debianmjukvara. Tillgängligt i 70 språk och
med stöd för en mängd datortyper, kallar Debian sig självt för
<q>det universella operativsystemet</q>.
</p>

<h2>Om DebConf</h2>

<p>
DebConf är Debianprojektets utvecklarkonferens. Utöver ett fullt schema med
tekniska, sociala och policyföreläsningar, ger DebConf en möjlighet för
utvecklare, bidragslämnare och andra intresserade att mötas personligen
och jobba närmare tillsammans. Det har ägt rum årligen sedan 2000 på
så vitt skilda platser som Scotland, Argentina, Bosnien och Hercegovina.
Mer information om DebConf finns tillgänglig på 
<a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>Om Lenovo</h2>

<p>
Som en global teknologiledare som tillverkar en bredd av anslutna produkter,
inklusive mobiltelefoner, plattor, PCs och arbetsstationer så väl som
AR/VR-enheter, smarta hem-/kontors-, och datacenterlösningar, förstår
<a href="https://www.lenovo.com">Lenovo</a> hur kritiskt det är med öppna
system och plattformar för en ansluten värld.
</p>

<h2>Om Infomaniak</h2>

<p>
<a href="https://www.infomaniak.com">Infomaniak</a> är Schweiz störa webbhostingföretag,
som även erbjuder backup- och lagringstjänster, lösningar för organisatörer av evenemang,
live-streaming och video on demand-tjänster.
De äger sina datacenter själva och alla element som är kritiska för
tjänsternas funktionalitet och produkter som tillhandahålls av företaget
(både mjukvara och hårdvara). 
</p>

<h2>Om Roche</h2>
<p>
<a href="https://code4life.roche.com/">Roche</a> är en stor internationell
läkemedelsleverantör och ett forskningsföretag som ägnar sig åt personlig
hälsovård. Mer än 100000 anställda över hela jorden arbetar för att lösa några
av dom största utmaningarna för mänskligheten med hjälp av vetenskap och
teknologi. Roche är starkt engagerad i offentligt finansierade samarbetsprojekt
inom forskning med andra industriella och akademiska parner och har stött
DebConf sedan 2017.
</p>

<h2>Om Amazon Web Services (AWS)</h2>
<p>
<a href="https://aws.amazon.com">Amazon Web Services (AWS)</a> är en av världens
mest omfattande och brett antagna molnplattform, som erbjuder över 175
fullt utrustade tjänster från datacenter globalt (i 77 tillgänglighetszoner
inom 24 geografiska regioner). AWS-kunder inkluderar de snabbast växande
start-ups, de största företagsjättarna och ledande myndigheter.
</p>

<h2>Om Google</h2>

<p>
<a href="https://google.com/">Google</a> är ett av världens största teknologiföretag,
som tillhandahåller en bredd av Internet-relaterade tjänster och produkter så
som online-reklamteknologier, sökning, molnplattformar, mjukvara, och hårdvara.
</p>

<p>
Google har stött Debian genom att sponsra DebConf under mer än tio år,
och är även en Debianpartner som sponsrar delar av 
<a href="https://salsa.debian.org">Salsa</a>'s continuous integration-infrastruktur
inom Google Cloud-plattformen.
</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information vänligen besök DebConf20s webbplats på
<a href="https://debconf20.debconf.org/">https://debconf20.debconf.org/</a>
eller skicka e-post till &lt;press@debian.org&gt;.</p>
