<define-tag pagetitle>Uppdaterad Debian 10; 10.11 utgiven</define-tag>
<define-tag release_date>2021-10-09</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="d9e5cd3d7df23feb17458b95c465e062e9cd6e5a" maintainer="Andreas Rönnquist"

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.11</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debianprojektet presenterar stolt sin elfte uppdatering till dess
gamla stabila utgåva Debian <release> (med kodnamnet <q><codename></q>). 
Denna punktutgåva lägger huvudsakligen till rättningar för säkerhetsproblem,
tillsammans med ytterligare rättningar för allvarliga problem. Säkerhetsbulletiner
har redan publicerats separat och refereras när de finns tillgängliga.</p>

<p>Vänligen notera att punktutgåvan inte innebär en ny version av Debian 
<release> utan endast uppdaterar några av de inkluderade paketen. Det behövs
inte kastas bort gamla media av <q><codename></q>. Efter installationen
kan paket uppgraderas till de aktuella versionerna genom att använda en uppdaterad
Debianspegling..</p>

<p>De som frekvent installerar uppdateringar från security.debian.org kommer inte att behöva
uppdatera många paket, och de flesta av sådana uppdateringar finns
inkluderade i punktutgåvan.</p>

<p>Nya installationsavbildningar kommer snart att finnas tillgängliga på de vanliga platserna.</p>

<p>En uppgradering av en existerande installation till denna revision kan utföras genom att
peka pakethanteringssystemet på en av Debians många HTTP-speglingar.
En utförlig lista på speglingar finns på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Blandade felrättningar</h2>

<p>Denna uppdatering av den gamla stabila utgåvan lägger till några viktiga felrättningar till följande paket:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction atftp "Fix buffer overflow [CVE-2021-41054]">
<correction base-files "Update for the 10.11 point release">
<correction btrbk "Fix arbitrary code execution issue [CVE-2021-38173]">
<correction clamav "New upstream stable release; fix clamdscan segfaults when --fdpass and --multipass are used together with ExcludePath">
<correction commons-io "Fix path traversal issue [CVE-2021-29425]">
<correction cyrus-imapd "Fix denial-of-service issue [CVE-2021-33582]">
<correction debconf "Check that whiptail or dialog is actually usable">
<correction debian-installer "Rebuild against buster-proposed-updates; update Linux ABI to 4.19.0-18">
<correction debian-installer-netboot-images "Rebuild against buster-proposed-updates">
<correction distcc "Fix GCC cross-compiler links in update-distcc-symlinks and add support for clang and CUDA (nvcc)">
<correction distro-info-data "Update included data for several releases">
<correction dwarf-fortress "Remove undistributable prebuilt shared libraries from the source tarball">
<correction espeak-ng "Fix using espeak with mbrola-fr4 when mbrola-fr1 is not installed">
<correction gcc-mingw-w64 "Fix gcov handling">
<correction gthumb "Fix heap-based buffer overflow issue [CVE-2019-20326]">
<correction hg-git "Fix test failures with recent git versions">
<correction htslib "Fix autopkgtest on i386">
<correction http-parser "Fix HTTP request smuggling issue [CVE-2019-15605]">
<correction irssi "Fix use after free issue when sending SASL login to the server [CVE-2019-13045]">
<correction java-atk-wrapper "Also use dbus to detect accessibility being enabled">
<correction krb5 "Fix KDC null dereference crash on FAST request with no server field [CVE-2021-37750]; fix memory leak in krb5_gss_inquire_cred">
<correction libdatetime-timezone-perl "New upstream stable release; update DST rules for Samoa and Jordon; confirmation of no leap andra on 2021-12-31">
<correction libpam-tacplus "Prevent shared secrets from being added in plaintext to the system log [CVE-2020-13881]">
<correction linux "<q>proc: Track /proc/$pid/attr/ opener mm_struct</q>, fixing issues with lxc-attach; new upstream stable release; increase ABI version to 18; [rt] Update to 4.19.207-rt88; usb: hso: fix error handling code of hso_create_net_device [CVE-2021-37159]">
<correction linux-latest "Update to 4.19.0-18 kernel ABI">
<correction linux-signed-amd64 "<q>proc: Track /proc/$pid/attr/ opener mm_struct</q>, fixing issues with lxc-attach; new upstream stable release; increase ABI version to 18; [rt] Update to 4.19.207-rt88; usb: hso: fix error handling code of hso_create_net_device [CVE-2021-37159]">
<correction linux-signed-arm64 "<q>proc: Track /proc/$pid/attr/ opener mm_struct</q>, fixing issues with lxc-attach; new upstream stable release; increase ABI version to 18; [rt] Update to 4.19.207-rt88; usb: hso: fix error handling code of hso_create_net_device [CVE-2021-37159]">
<correction linux-signed-i386 "<q>proc: Track /proc/$pid/attr/ opener mm_struct</q>, fixing issues with lxc-attach; new upstream stable release; increase ABI version to 18; [rt] Update to 4.19.207-rt88; usb: hso: fix error handling code of hso_create_net_device [CVE-2021-37159]">
<correction mariadb-10.3 "New upstream stable release; security fixes [CVE-2021-2389 CVE-2021-2372]; fix Perl executable path in scripts">
<correction modsecurity-crs "Fix request body bypass issue [CVE-2021-35368]">
<correction node-ansi-regex "Fix regular expression-based denial of service issue [CVE-2021-3807]">
<correction node-axios "Fix regular expression-based denial of service issue [CVE-2021-3749]">
<correction node-jszip "Use a null prototype object for this.files [CVE-2021-23413]">
<correction node-tar "Remove non-directory paths from the directory cache [CVE-2021-32803]; strip absolute paths more comprehensively [CVE-2021-32804]">
<correction nvidia-cuda-toolkit "Fix setting of NVVMIR_LIBRARY_DIR on ppc64el">
<correction nvidia-graphics-drivers "New upstream stable release; fix denial of service issues [CVE-2021-1093 CVE-2021-1094 CVE-2021-1095]; nvidia-driver-libs: Add Recommends: libnvidia-encode1">
<correction nvidia-graphics-drivers-legacy-390xx "New upstream stable release; fix denial of service issues [CVE-2021-1093 CVE-2021-1094 CVE-2021-1095]; nvidia-legacy-390xx-driver-libs: Add Recommends: libnvidia-legacy-390xx-encode1">
<correction postgresql-11 "New upstream stable release; fix mis-planning of repeated application of a projection step [CVE-2021-3677]; disallow SSL renegotiation more completely">
<correction proftpd-dfsg "Fix <q>mod_radius leaks memory contents to radius server</q>, <q>cannot disable client-initiated renegotiation for FTPS</q>, navigation into symlinked directories, mod_sftp crash when using pubkey-auth with DSA keys">
<correction psmisc "Fix regression in killall not matching process with names longer than 15 characters">
<correction python-uflash "Update firmware URL">
<correction request-tracker4 "Fix login timing side-channel attack issue [CVE-2021-38562]">
<correction ring "Fix denial of service issue in the embedded copy of pjproject [CVE-2021-21375]">
<correction sabnzbdplus "Prevent directory escape in renamer function [CVE-2021-29488]">
<correction shim "Add arm64 patch to tweak section layout and stop crashing problems; in insecure mode, don't abort if we can't create the MokListXRT variable; don't abort on grub installation failures; warn instead">
<correction shim-helpers-amd64-signed "Add arm64 patch to tweak section layout and stop crashing problems; in insecure mode, don't abort if we can't create the MokListXRT variable; don't abort on grub installation failures; warn instead">
<correction shim-helpers-arm64-signed "Add arm64 patch to tweak section layout and stop crashing problems; in insecure mode, don't abort if we can't create the MokListXRT variable; don't abort on grub installation failures; warn instead">
<correction shim-helpers-i386-signed "Add arm64 patch to tweak section layout and stop crashing problems; in insecure mode, don't abort if we can't create the MokListXRT variable; don't abort on grub installation failures; warn instead">
<correction shim-signed "Work around boot-breaking issues on arm64 by including an older known working version of unsigned shim on that platform; switch arm64 back to using a current unsigned build; add arm64 patch to tweak section layout and stop crashing problems; in insecure mode, don't abort if we can't create the MokListXRT variable; don't abort on grub installation failures; warn instead">
<correction shiro "Fix authentication bypass issues [CVE-2020-1957 CVE-2020-11989 CVE-2020-13933 CVE-2020-17510]; update Spring Framework compatibility patch; support Guice 4">
<correction tzdata "Update DST rules for Samoa and Jordan; confirm the absence of a leap andra on 2021-12-31">
<correction ublock-origin "New upstream stable release; fix denial of service issue [CVE-2021-36773]">
<correction ulfius "Ensure memory is initialised before use [CVE-2021-40540]">
<correction xmlgraphics-commons "Fix Server-Side Request Forgery issue [CVE-2020-11988]">
<correction yubikey-manager "Add missing dependency on python3-pkg-resources to yubikey-manager">
</table>


<h2>Säkerhetsuppdateringar</h2>


<p>Denna revision lägger till följande säkerhetsuppdateringar till den gamla stabila utgåvan.
Säkerhetsgruppen har redan släppt bulletiner för alla dessa
uppdateringar:</p>

<table border=0>
<tr><th>Bulletin-ID</th>  <th>Paket</th></tr>
<dsa 2021 4842 thunderbird>
<dsa 2021 4866 thunderbird>
<dsa 2021 4876 thunderbird>
<dsa 2021 4897 thunderbird>
<dsa 2021 4927 thunderbird>
<dsa 2021 4931 xen>
<dsa 2021 4932 tor>
<dsa 2021 4933 nettle>
<dsa 2021 4934 intel-microcode>
<dsa 2021 4935 php7.3>
<dsa 2021 4936 libuv1>
<dsa 2021 4937 apache2>
<dsa 2021 4938 linuxptp>
<dsa 2021 4939 firefox-esr>
<dsa 2021 4940 thunderbird>
<dsa 2021 4941 linux-signed-amd64>
<dsa 2021 4941 linux-signed-arm64>
<dsa 2021 4941 linux-signed-i386>
<dsa 2021 4941 linux>
<dsa 2021 4942 systemd>
<dsa 2021 4943 lemonldap-ng>
<dsa 2021 4944 krb5>
<dsa 2021 4945 webkit2gtk>
<dsa 2021 4946 openjdk-11-jre-dcevm>
<dsa 2021 4946 openjdk-11>
<dsa 2021 4947 libsndfile>
<dsa 2021 4948 aspell>
<dsa 2021 4949 jetty9>
<dsa 2021 4950 ansible>
<dsa 2021 4951 bluez>
<dsa 2021 4952 tomcat9>
<dsa 2021 4953 lynx>
<dsa 2021 4954 c-ares>
<dsa 2021 4955 libspf2>
<dsa 2021 4956 firefox-esr>
<dsa 2021 4957 trafficserver>
<dsa 2021 4958 exiv2>
<dsa 2021 4959 thunderbird>
<dsa 2021 4961 tor>
<dsa 2021 4962 ledgersmb>
<dsa 2021 4963 openssl>
<dsa 2021 4964 grilo>
<dsa 2021 4967 squashfs-tools>
<dsa 2021 4969 firefox-esr>
<dsa 2021 4970 postorius>
<dsa 2021 4971 ntfs-3g>
<dsa 2021 4973 thunderbird>
<dsa 2021 4974 nextcloud-desktop>
<dsa 2021 4975 webkit2gtk>
<dsa 2021 4979 mediawiki>
</table>


<h2>Borttagna paket</h2>

<p>Följande paket har tagits bort på grund av omständigheter utom vår kontroll:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction birdtray "Incompatible with newer Thunderbird versions">
<correction libprotocol-acme-perl "Only supports obsolete ACME version 1">

</table>

<h2>Debianinstalleraren</h2>
<p>Installeraren har uppdaterats för att inkludera rättningarna som har inkluderats i den
gamla stabila utgåvan med denna punktutgåva.</p>

<h2>URLer</h2>

<p>Den fullständiga listan på paket som har förändrats i denna revision:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuella gamla stabila utgåvan:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Föreslagna uppdateringar till den gamla stabila utgåvan:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Information om den gamla stabila utgåvan (versionsfakta, kända problem osv.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Säkerhetsbulletiner och information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Om Debian</h2>

<p>Debianprojektet är en grupp utvecklare av Fri mjukvara som
donerar sin tid och kraft för att producera det helt
fria operativsystemet Debian.</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, vänligen besök Debians webbplats på
<a href="$(HOME)/">https://www.debian.org/</a>, skicka e-post till
&lt;press@debian.org&gt;, eller kontakta gruppen för stabila utgåvor på
&lt;debian-release@lists.debian.org&gt;.</p>


