#use wml::debian::template title="Debian på skrivbordet" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="5946007f07ed802a4ea96fe8dfa5ef2681f41b18"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#philosophy">Vår filosofi</a></li>
<li><a href="#help">Hur du kan hjälpa till</a></li>
<li><a href="#join">Gå med oss</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian Desktop är en grupp
frivilliga som vill skapa det bästa möjliga operativsystemet för hem- och
företagsanvändning. Vårt motto: <q>Mjukvara som bara fungerar.</q> Vårt mål:
att få in Debian, GNU och Linux i <span lang="en">mainstream</span>-världen.</p>
</aside>

<h2><a id="philosophy">Vår filosofi</a></h2>

<p>
Vi erkänner att många
<a href="https://wiki.debian.org/DesktopEnvironment">Skrivbordsmiljöer</a>
finns och kommer att stödja deras användning - detta inkluderar att
säkerställa att dom fungerar bra på Debian. Vårt mål är att göra
de grafiska användargränssnitten lättanvända för nybörjare och samtidigt
tillåta avancerade användare och experter att ställa in saker så som dom
vill ha dom.
</p>

<p>
Vi kommer försöka se till att programvara konfigureras för den vanligaste
skrivbordsanvändningen. Exempelvis bör standardkontot som skapas under
installationen ha tillgång till att spela ljud och video, utskrifter,
och hantera systemet mha sudo. Vi kommer också att försöka se till att de
frågor som ställs till användaren via 
<a href="https://wiki.debian.org/debconf">debconf</a> (Debians 
konfigurationshanteringssystem) är så få som möjligt. Det finns inget behov
av att presentera komplicerade tekniska detaljer under installationen. Istället
kommer vi försöka säkerställa att debconf-frågor är vettiga för användaren.
En nybörjare kanske inte ens förstår vad frågorna handlar om. En expert,
å andra sidan kan istället konfigurera skrivbordsmiljön efter att installationen
är färdig.
</p>

<h2><a id="help">Hur du kan hjälpa till</a></h2>

<p>
Vi söker efter motiverat folk som kan få saker gjort. Du behöver inte vara
en Debianutvecklare (DD) för att skicka patchar eller skapa paket.
Debian Dekstop-gruppen kommer att säkerställas att ditt arbete integreras.
Så, här hittar du några saker som du kan hjälpa till med:
</p>

<ul>
  <li>Testa standardskrivbordsmiljön (eller någon av de andra skrivbordsmiljöerna)
      för de kommande utgåvan. Hämta en av <a
      href="$(DEVEL)/debian-installer/">uttestningsavbildningarna</a> och skicka
      feedback till sändlistan <a 
      href="https://lists.debian.org/debian-desktop/">debian-desktop</a>.</li>
  <li>Gå med <a href="https://wiki.debian.org/DebianInstaller/Team">Debian 
      Installer-gruppen</a> och hjälp till att förbättra <a
      href="$(DEVEL)/debian-installer/">debian-installeraren</a> –
      GTK+-gränssnittet behöver dig.</li>
  <li>Du kan hjälpa <a 
      href="https://wiki.debian.org/Teams/DebianGnome">Debian GNOME-gruppen</a>, 
      <a href="https://qt-kde-team.pages.debian.net/"> Debian Qt/KDE 
      Maintainers och Debian KDE Extras-gruppen</a>, eller <a 
      href="https://salsa.debian.org/xfce-team/">Debian Xfce-gruppen</a> med
      paketering, felrättning, dokumentation, testning osv.</li>
  <li>Hjälp till att förbättra <a href="https://packages.debian.org/debconf">debconf</a> 
      genom att sänka prioriteten för frågor eller ta bort onödiga frågor från
      paket. Gör dom nödvändiga debconf-frågorna lättare att förstå.</li>
  <li>Har du designerkompetens? I så fall, varför inte jobba på <a 
      href="https://wiki.debian.org/DebianDesktop/Artwork">Debian Desktop 
      Artwork</a>.</li>
</ul>

<h2><a id="join">Gå med oss</a></h2>

<aside class="light">
  <span class="fa fa-users fa-4x"></span>
</aside>

<ul>
  <li><strong>Wiki:</strong>Besök vår <a 
      href="https://wiki.debian.org/DebianDesktop">DebianDesktop</a> Wiki (några 
      artiklar kan vara inaktuella).</li>
  <li><strong>Sändlistor:</strong> Diskutera med oss på <a 
      href="https://lists.debian.org/debian-desktop/">debian-desktop</a>-sändlistan.</li>
  <li><strong>IRC-kanal:</strong> Chatta med oss på IRC. Gå med
      kanalen #debian-desktop på <a href="http://oftc.net/">OFTC 
      IRC</a> (irc.debian.org)</li>
</ul>

