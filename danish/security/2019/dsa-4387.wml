#use wml::debian::translation-check translation="19fdc288616ee3bfe6ee122b16cd10940121ffb2" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Harry Sintonen fra F-Secure Corporation opdagede adskillige sårbarheder i 
OpenSSH, en implementering af SSH-protokolprogrampakken.  Alle sårbarhederne 
blev findet i scp-klientens implementering af SCP-protokollen.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20685">CVE-2018-20685</a>

    <p>På grund af ukorrekt validering af mappenavn, tillod scp-klienten at 
    servere kunne ændre målmappens rettigheder, ved at anvende tomme eller 
    punktum-mappenavne.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6109">CVE-2019-6109</a>

    <p>På grund af manglende tegnindkapsling i fremgangsvisningen, kunne 
    objektnavnet anvendes til at manipulere med klientens uddata, for eksempel 
    til at bruge ANSI-koder til at skjule yderligere filer i en 
    overførsel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6111">CVE-2019-6111</a>

    <p>På grund af utilstrækkelig validering af inddata i stinavne, sendt af en 
    server, i scp-klienten, kunne en ondsindet server foretage vilkårlige 
    filoverskrivninger i målmappen.  Hvis en rekursiv valgmulighed (-r) blev 
    benyttet, kunne serveren også manipulere med undermapper.</p>

    <p>Kontrollen der er tilføjet i denne version, kan føre til regression, hvis 
    klienten og serveren har forskelle i wildcardudvidelsesreglerne.  Hvis 
    der stoles på serveren til det formål, kan kontrollere deaktivere med en ny 
    valgmulighed, -T, i scp-klienten.</p></li>

</ul>

<p>I den stabile distribution (stretch), er disse problemer rettet i
version 1:7.4p1-10+deb9u5.</p>

<p>Vi anbefaler at du opgraderer dine openssh-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende openssh, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/openssh">\
https://security-tracker.debian.org/tracker/openssh</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4387.data"
