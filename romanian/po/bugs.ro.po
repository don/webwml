# Jobava <jobaval10n@gmail.com>, 2015.
# Cristian Silaghi <cristian.silaghi@mozilla.ro >, 2015.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-09-20 12:33+0100\n"
"Last-Translator: Jobava <jobaval10n@gmail.com>\n"
"Language-Team: Romanian <debian-l10n-romanian@lists.debian.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 1.8.8\n"

#: ../../english/Bugs/pkgreport-opts.inc:17
msgid "in package"
msgstr "în pachetul"

#: ../../english/Bugs/pkgreport-opts.inc:20
#: ../../english/Bugs/pkgreport-opts.inc:60
#: ../../english/Bugs/pkgreport-opts.inc:94
msgid "tagged"
msgstr "etichetate"

#: ../../english/Bugs/pkgreport-opts.inc:23
#: ../../english/Bugs/pkgreport-opts.inc:63
#: ../../english/Bugs/pkgreport-opts.inc:97
msgid "with severity"
msgstr "cu severitatea"

#: ../../english/Bugs/pkgreport-opts.inc:26
msgid "in source package"
msgstr "în pachetul sursă"

#: ../../english/Bugs/pkgreport-opts.inc:29
msgid "in packages maintained by"
msgstr "în pachete menținute de"

#: ../../english/Bugs/pkgreport-opts.inc:32
msgid "submitted by"
msgstr "trimise de"

#: ../../english/Bugs/pkgreport-opts.inc:35
msgid "owned by"
msgstr "deținute de"

#: ../../english/Bugs/pkgreport-opts.inc:38
msgid "with status"
msgstr "cu starea"

#: ../../english/Bugs/pkgreport-opts.inc:41
msgid "with mail from"
msgstr "cu e-mail de la"

#: ../../english/Bugs/pkgreport-opts.inc:44
msgid "newest bugs"
msgstr "cele mai noi defecte"

#: ../../english/Bugs/pkgreport-opts.inc:57
#: ../../english/Bugs/pkgreport-opts.inc:91
msgid "with subject containing"
msgstr "cu subiect conținând"

#: ../../english/Bugs/pkgreport-opts.inc:66
#: ../../english/Bugs/pkgreport-opts.inc:100
msgid "with pending state"
msgstr "cu stare „în așteptare”"

#: ../../english/Bugs/pkgreport-opts.inc:69
#: ../../english/Bugs/pkgreport-opts.inc:103
msgid "with submitter containing"
msgstr "cu raportor conținând"

#: ../../english/Bugs/pkgreport-opts.inc:72
#: ../../english/Bugs/pkgreport-opts.inc:106
msgid "with forwarded containing"
msgstr "cu redirecționat conținând"

#: ../../english/Bugs/pkgreport-opts.inc:75
#: ../../english/Bugs/pkgreport-opts.inc:109
msgid "with owner containing"
msgstr "cu proprietar conținând"

#: ../../english/Bugs/pkgreport-opts.inc:78
#: ../../english/Bugs/pkgreport-opts.inc:112
msgid "with package"
msgstr "cu pachetul"

#: ../../english/Bugs/pkgreport-opts.inc:122
msgid "normal"
msgstr "normal"

#: ../../english/Bugs/pkgreport-opts.inc:125
msgid "oldview"
msgstr "oldview"

#: ../../english/Bugs/pkgreport-opts.inc:128
msgid "raw"
msgstr "brut"

#: ../../english/Bugs/pkgreport-opts.inc:131
msgid "age"
msgstr "vârstă"

#: ../../english/Bugs/pkgreport-opts.inc:137
msgid "Repeat Merged"
msgstr "Repetă fuzionate"

#: ../../english/Bugs/pkgreport-opts.inc:138
msgid "Reverse Bugs"
msgstr "Inversează rapoarte"

#: ../../english/Bugs/pkgreport-opts.inc:139
msgid "Reverse Pending"
msgstr "Inversează așteaptă încărcare"

#: ../../english/Bugs/pkgreport-opts.inc:140
msgid "Reverse Severity"
msgstr "Inversează severitate"

#: ../../english/Bugs/pkgreport-opts.inc:141
msgid "No Bugs which affect packages"
msgstr "Fără rapoarte ce afectează alte pachete"

#: ../../english/Bugs/pkgreport-opts.inc:143
msgid "None"
msgstr "Niciunul"

#: ../../english/Bugs/pkgreport-opts.inc:144
msgid "testing"
msgstr "testare"

#: ../../english/Bugs/pkgreport-opts.inc:145
msgid "oldstable"
msgstr "anterioară stabilă"

#: ../../english/Bugs/pkgreport-opts.inc:146
msgid "stable"
msgstr "stabilă"

#: ../../english/Bugs/pkgreport-opts.inc:147
msgid "experimental"
msgstr "experimental"

#: ../../english/Bugs/pkgreport-opts.inc:148
msgid "unstable"
msgstr "instabilă"

#: ../../english/Bugs/pkgreport-opts.inc:152
msgid "Unarchived"
msgstr "Nearhivate"

#: ../../english/Bugs/pkgreport-opts.inc:155
msgid "Archived"
msgstr "Arhivate"

#: ../../english/Bugs/pkgreport-opts.inc:158
msgid "Archived and Unarchived"
msgstr "Arhivate și nearhivate"

#~ msgid "Flag"
#~ msgstr "Opțiuni:"

#~ msgid "active bugs"
#~ msgstr "probleme nerezolvate"

#~ msgid "display merged bugs only once"
#~ msgstr "arată problemele numai o dată"

#~ msgid "no ordering by status or severity"
#~ msgstr "fără ordine"

#~ msgid "don't show table of contents in the header"
#~ msgstr "nu arăta indexul în antet"

#~ msgid "don't show statistics in the footer"
#~ msgstr "nu arăta statistici în josul paginii"

#~ msgid "bugs"
#~ msgstr "probleme"

#~ msgid "open"
#~ msgstr "deschis"

#~ msgid "forwarded"
#~ msgstr "redirecționat"

#~ msgid "pending"
#~ msgstr "pe cale de a fi rezolvat"

#~ msgid "fixed"
#~ msgstr "rezolvat"

#~ msgid "done"
#~ msgstr "terminat"

#~ msgid "Include status:"
#~ msgstr "Inclus:"

#~ msgid "Exclude status:"
#~ msgstr "Exclus"

#~ msgid "critical"
#~ msgstr "critic"

#~ msgid "grave"
#~ msgstr "grav"

#~ msgid "serious"
#~ msgstr "serios"

#~ msgid "important"
#~ msgstr "important"

#~ msgid "minor"
#~ msgstr "minor"

#~ msgid "wishlist"
#~ msgstr "sub examinare"

#~ msgid "Include severity:"
#~ msgstr "Include severitatea:"

#~ msgid "Exclude severity:"
#~ msgstr "Exclude severitatea:"

#~ msgid "potato"
#~ msgstr "potato"

#~ msgid "woody"
#~ msgstr "woody"

#~ msgid "sarge-ignore"
#~ msgstr "sarge-ignore"

#, fuzzy
#~ msgid "etch"
#~ msgstr "patch"

#, fuzzy
#~ msgid "etch-ignore"
#~ msgstr "sarge-ignore"

#, fuzzy
#~ msgid "lenny-ignore"
#~ msgstr "sarge-ignore"

#~ msgid "sid"
#~ msgstr "sid"

#~ msgid "confirmed"
#~ msgstr "confirmat"

#~ msgid "d-i"
#~ msgstr "d-i"

#~ msgid "fixed-in-experimental"
#~ msgstr "fixat-in-experimental"

#~ msgid "fixed-upstream"
#~ msgstr "fixat-upstream"

#~ msgid "help"
#~ msgstr "ajutor"

#~ msgid "moreinfo"
#~ msgstr "mai multe informatii"

#~ msgid "patch"
#~ msgstr "patch"

#~ msgid "security"
#~ msgstr "securitate"

#~ msgid "unreproducible"
#~ msgstr "imposibil de reprodus"

#~ msgid "upstream"
#~ msgstr "upstream"

#~ msgid "wontfix"
#~ msgstr "nu va fi rezolvat"

#~ msgid "Include tag:"
#~ msgstr "Include tag:"

#~ msgid "Exclude tag:"
#~ msgstr "Exclude tag:"
