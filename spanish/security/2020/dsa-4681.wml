#use wml::debian::translation-check translation="9366447bdb2abdccc3962d87981f5cce43bf471c"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto las siguientes vulnerabilidades en el motor web
webkit2gtk:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3885">CVE-2020-3885</a>

    <p>Ryan Pickren descubrió que se podía procesar incorrectamente la URL de un
    fichero.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3894">CVE-2020-3894</a>

    <p>Sergei Glazunov descubrió que una condición de carrera podía permitir que una
    aplicación leyera memoria restringida.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3895">CVE-2020-3895</a>

    <p>grigoritchy descubrió que el procesado de contenido web preparado
    maliciosamente podía dar lugar a ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3897">CVE-2020-3897</a>

    <p>Brendan Draper descubrió que atacantes en ubicaciones remotas podían
    provocar ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3899">CVE-2020-3899</a>

    <p>OSS-Fuzz descubrió que atacantes en ubicaciones remotas podían provocar
    ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3900">CVE-2020-3900</a>

    <p>Dongzhuo Zhao descubrió que el procesado de contenido web preparado
    maliciosamente podía dar lugar a ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3901">CVE-2020-3901</a>

    <p>Benjamin Randazzo descubrió que el procesado de contenido web preparado
    maliciosamente podía dar lugar a ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3902">CVE-2020-3902</a>

    <p>Yigit Can Yilmaz descubrió que el procesado de contenido web preparado
    maliciosamente podía dar lugar a un ataque de ejecución de scripts entre sitios («cross site scripting»).</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 2.28.2-2~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de webkit2gtk.</p>

<p>Para información detallada sobre el estado de seguridad de webkit2gtk, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4681.data"
