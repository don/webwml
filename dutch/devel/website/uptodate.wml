#use wml::debian::template title="De vertaling van de website up-to-date houden"
#use wml::debian::translation-check translation="8f2dd37edbf6287df4029e3f234798efbcce2862"

<P>Omdat webpagina's niet statisch zijn, is het een goed idee om bij te
houden naar welke versie van het origineel een bepaalde vertaling verwijst,
en deze informatie te gebruiken om te controleren welke pagina's sinds
de laatste vertaling gewijzigd werden. Deze informatie moet bovenin het
document opgenomen worden (evenwel onder alle andere "use"-kopregels) in
deze vorm:

<pre>
\#use wml::debian::translation-check translation="git_commit_hash"
</pre>

<p>waarbij <var>git_commit_hash</var> de hashcode is van de git-vastlegging
van het originele (Engelse) bestand, waarvan het vertaalde
bestand de vertaling is. U kunt de details over die specifieke vastlegging
verkrijgen met het hulpmiddel <code>git show</code>: <code>git show
&lt;git_commit_hash&gt;</code>. Indien u het script <kbd>copypage.pl</kbd>
uit de map webwml gebruikt, wordt de <code>translation-check</code>-regel
automatisch toegevoegd aan de eerste versie van uw vertaalde pagina, en
deze verwijst daarbij naar de op dat ogenblik bestaande versie van het
originele bestand. </p>

<p>Sommige vertalingen worden mogelijk lange tijd niet bijgewerkt, zelfs al
werd de pagina in de originele taal (Engels) gewijzigd. Door het mechanisme van
inhoudsonderhandeling (content negotiation) kan het zijn dat de lezer
van de vertaalde pagina zich hiervan niet bewust is en belangrijke informatie
mist, welke in nieuwere versies van het origineel werd geïntroduceerd. Het
<code>translation-check</code>-model bevat code om te controleren of uw
vertaling verouderd is en geeft een passend bericht weer, waarmee de gebruiker
daarover gewaarschuwd wordt. </p>

<P>U kunt nog andere parameters gebruiken op de
<code>translation-check</code>-regel:

<dl>
 <dt><code>original="<var>taal</var>"</code>
 <dd>waarbij <var>taal</var> de taal is waaruit u vertaalt, wanneer dit
 geen Engels is. De naam moet overeenkomen met een map op het basisniveau
 van het VCS en met de naam in het <code>languages.wml</code>-model.

 <dt><code>mindelta="<var>getal</var>"</code>
 <dd>wat het maximale verschil in git-revisies definieert voordat de vertaling
 als <strong>oud</strong> wordt beschouwd. De standaardwaarde is
 <var>1</var>. Stel dit voor minder belangrijke pagina's in op <var>2</var>,
 wat betekent dat twee wijzigingen nodig zijn voordat de vertaling als
 oud wordt beschouwd.

 <dt><code>maxdelta="<var>getal</var>"</code>
 <dd>wat het maximale verschil in git-revisies definieert voordat de vertaling
 als <strong>verouderd</strong> wordt beschouwd. De standaardwaarde is
 <var>5</var>. Stel dit voor erg belangrijke pagina's in op een kleiner getal.
 Een waarde van <var>1</var> betekent dat elke verandering zal maken dat de
 vertaling als verouderd gemarkeerd wordt.
</dl>

<p>Het opvolgen van de ouderdom van vertalingen maakt het ons ook mogelijk om
<a href="stats/">vertalingsstatistieken</a> bij te houden, een rapport op te
stellen over verouderde vertalingen (samen met nuttige links naar het verschil
tussen bestanden), samen met een lijst van pagina's die helemaal niet vertaald
zijn. Dit is bedoeld om vertalers te helpen en nieuwe mensen aan te trekken
die willen helpen. </p>

<p>
Om te vermijden dat we onze gebruikers informatie bieden die te verouderd is,
zullen vertalingen die niet werden bijgewerkt binnen de zes maanden na de
wijziging van de originele pagina, automatisch worden gewist. Raadpleeg de
<a href="https://www.debian.org/devel/website/stats/">lijst met verouderde
vertalingen</a> om te weten welke pagina's het gevaar lopen gewist te worden.
</p>

<P>
Bovendien is in de map webwml/ het script <kbd>check_trans.pl</kbd> beschikbaar,
wat u een rapport laat zien over pagina's die een update nodig hebben:

<pre>
check_trans.pl <var>taal</var>
</pre>

<P>waarbij <var>taal</var> de naam is van de map die uw vertaling bevat,
bijv. "dutch".

<P>Pagina's waarvan de vertaling ontbreekt zullen weergegeven worden als
"<code>Missing <var>bestandsnaam</var></code>", en pagina's die niet
up-to-date zijn met het origineel zullen weergegeven worden als
"<code>NeedToUpdate <var>bestandsnaam</var> to version <var>XXXXXXX</var></code>".

<P>Indien u wilt zien wat er exact gewijzigd werd, kunt u de verschillen te
zien krijgen door de commandoregeloptie <kbd>-d</kbd> mee te geven aan
bovenstaand commando.</p>

<P>Indien u waarschuwingen over ontbrekende vertalingen wilt negeren (bijv.
voor oude nieuws-items) kunt u een bestand creëren met de naam
<code>.transignore</code> in de map waarvoor u de waarschuwingen wilt
onderdrukken, en waarin u elk bestand vermeldt dat u niet zult vertalen,
één bestand per regel.

<p>
Er is ook een gelijkaardig script beschikbaar voor de opvolging van de
vertaling van de beschrijving van de mailinglijsten. Lees het commentaar in
het script <code>check_desc_trans.pl</code> ter documentatie.
</p>
